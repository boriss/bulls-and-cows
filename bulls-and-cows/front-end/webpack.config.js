const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = options => {
  return {
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, 'public'),
      filename: 'bundle.js'
    },
    module: {
      rules: [
        {
          test: /.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                cacheDirectory: true,
              },
            },
          ],
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        }
      ],
    },
    devServer: {
      contentBase: "./public"
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, 'src', 'index.template.html'),
        filename: path.resolve(__dirname, 'public', 'index.html')
      })
    ]
  }
};
