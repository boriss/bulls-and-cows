package main

import (
	"fmt"
	"net/http"
	"regexp"
)

func handlerStatic(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Path")
	fmt.Println(r.URL.Path)
	// Path includes file extension
	var validFile = regexp.MustCompile(`^\/[.\/]*.+\..{1,}$`)
	if validFile.MatchString(r.URL.Path) {
		http.ServeFile(w, r, "/bin/public/"+r.URL.Path)
	} else {
		fmt.Println("Serving index")
		http.ServeFile(w, r, "/bin/public/index.html")
	}
}

func main() {
	http.Handle("/", http.HandlerFunc(handlerStatic))
	http.ListenAndServe(":9999", nil)
}
