import React, { Component } from 'react';
import './App.css';
import Terminal from 'terminal-in-react';
import { GoogleLogin } from './components/react-google-login-component/src/index.js';

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loggedIn: false,
            email: "",
            username: "",
            currentGameData: {
                type: "",
                id: ""
            }
        }
    }

    createSolo(args, print) {
        // TODO: Add check for already active game.
        if (args._.length != 1) {
            print('Invalid command!');
            return;
        }

        let body = JSON.stringify({
            player_id: this.state.email,
        });
        let that = this;
        console.log(this);
        fetch('http://dev.test.com:8888/api/solo/new', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: body,
            credentials: "include",
        }
        ).then(function(r) {
            r.json().then(function(data) {
                console.log(data);
                that.state.currentGameData.id = data;
                that.state.currentGameData.type = "solo";
                print('Successfully created a new game! Guess on!');
            });
        }, function(err) {
            console.log(err);
        });
    }

    createNpcDuel(number, print) {
        // TODO: Add check for already active game.
        if (args._.length != 2 || (''+args._[1]).length != 4) {
            print('Invalid command!');
            return;
        }

        let body = JSON.stringify({
            player_id: this.state.email,
            player_num: ''+args._[1],
        });
        let that = this;
        console.log(this);
        fetch('http://dev.test.com:8888/api/duel/npc/new', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: body,
            credentials: "include",
        }
        ).then(function(r) {
            r.json().then(function(data) {
                console.log(data);
                that.state.currentGameData.id = data;
                that.state.currentGameData.type = "duel";
                print('Successfully created a new game! Your turn to guess.');
            });
        }, function(err) {
            console.log(err);
        });
    }

    guess(number, print) {
        let body = JSON.stringify({
            game_id: this.state.currentGameData.id,
            player_id: this.state.email,
            number: number
        });
        let that = this;
        fetch(`http://dev.test.com:8888/api/${this.state.currentGameData.type}/guess`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: body,
            credentials: "include",
        }
        ).then(function(r) {
            r.json().then(function(data) {
                console.log(data);
                if (!data.won) {
                print(`Your guess (${number}) got ${data.bulls} bulls and ${data.cows} cows!`);
                } else {
                    that.state.currentGameData.id = '';
                    print('You won!');
                }
            });
        }, function(err) {
            console.log(err);
        });
    }

    responseGoogle(googleUser) {
        let id_token = googleUser.getAuthResponse().id_token,
            that = this,
            profile = googleUser.getBasicProfile();
        let email = profile.getEmail(),
            name = profile.getName();


        let bearer = 'Bearer ' + id_token,
            body = JSON.stringify({
                name: name,
            });
        fetch('http://dev.test.com:8888/auth/signin', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': bearer,
            },
            body: body,
            credentials: "include",
        }).then(function(r) {
            // Check for status etc...
            that.state.loggedIn = true;
            that.state.email = email;
            that.state.name = name;
        }, function(err) {
            console.log(err);
        });
    }

    render() {
        return (
            <div className="App"
                style={{
                    height: "100%"
                }}
            >
                <GoogleLogin socialId="736740107053-clkvf4v8tco5j0pf9bmme1f77gmtifft.apps.googleusercontent.com"
                        className="google-login"
                        scope="profile"
                        responseHandler={this.responseGoogle.bind(this)}
                        buttonText="Login With Google"/>
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        height: "100%",
                        width: "100%",
                    }}>
                    <Terminal
                        color='green'
                        backgroundColor='black'
                        barColor='black'
                        WatchConsoleLogging='false'
                        style={{ fontWeight: "bold", fontSize: "1em", height: "100%", width: "100%"}}
                        commands={{
                            create: {
                                method: (args, print, runCommand) => {
                                    switch (args._[0]) {
                                        case "npc-duel":
                                            print('Creating!');
                                            this.createNpcDuel(args, print);
                                            break;
                                        case "challenge": 
                                            print('Creating!');
                                            this.createSolo(args, print);
                                            break;
                                        case "player-duel":
                                            break;
                                    }
                                },
                                options: [ {
                                    name: 'create',
                                    description: 'npc-duel - Create a new duel vs a bot.\n        player-duel [email] - create a new duel vs another player',
                                    defaultValue: 'npc-duel',
                                } ],
                            },
                            guess: {
                                method: (args, print, runCommand) => {
                                    if (args._.length != 1 || (''+args._[0]).length != 4) {
                                        print('Invalid command!');
                                        return;
                                    }
                                    this.guess(''+args._[0], print);
                                },
                                options: [ {
                                    name: 'create',
                                    description: 'npc-duel - Create a new duel vs a bot.\n        player-duel [email] - create a new duel vs another player',
                                    defaultValue: 'npc-duel',
                                } ],
                            },
                            showmsg: this.showMsg,
                            popup: () => alert('Terminal in React')
                        }}
                        descriptions={{
                            create: {},
                            showmsg: 'shows a message',
                            alert: 'alert', popup: 'alert'
                        }}
                        msg='You can write anything here. Example - Hello! My name is Foo and I like Bar.'/>
                </div>
            </div>
        );
    }
}

export default App;
