package data

import (
	"sync"
	"time"

	log "github.com/Sirupsen/logrus"
	"gopkg.in/mgo.v2"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
)

var (
	dbSession *mgo.Session
	once      sync.Once
)

func init() {
	once.Do(func() {
		log.Info(config.Main)
		dbSession = getDBSession(config.Main.DBAddress)
	})
}

func getDBSession(host string) *mgo.Session {
	log.Info("Dialing mongo ", host)
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:  []string{host},
		Direct: false,
	}

	mongoChan := make(chan *mgo.Session)
	go func() {
		session, err := mgo.DialWithInfo(mongoDBDialInfo)
		if err != nil {
			log.WithFields(log.Fields{
				"error": err,
			}).Fatal("Error dialing mongo.")
		} else {
			mongoChan <- session
		}
	}()

	select {
	case session := <-mongoChan:
		log.Info("Done dialing mongo.")
		return session
	case <-time.After(time.Second * 10):
		log.Fatal("Timeout dialing mongo.")
	}

	return nil
}
