package data

import (
	"gopkg.in/mgo.v2/bson"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/models"
)

func CreateUser(email, name string) error {
	// Validate data
	user := models.User{
		Email: email,
		Name:  name,
	}

	session := dbSession.Clone()
	defer session.Close()

	err := session.
		DB(config.Main.DBName).
		C(config.UsersCollection).
		Insert(user)

	return err
}

func GetUserByEmail(email string) (*models.User, error) {
	// Validate data
	user := &models.User{}

	session := dbSession.Clone()
	defer session.Close()

	err := session.
		DB(config.Main.DBName).
		C(config.UsersCollection).
		Find(bson.M{"email": email}).
		One(user)

	return user, err
}
