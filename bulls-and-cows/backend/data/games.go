package data

import (
	"gopkg.in/mgo.v2/bson"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/models"

	log "github.com/Sirupsen/logrus"
)

func CreateGame(game interface{}, collection string) error {
	session := dbSession.Clone()
	defer session.Close()
	err := session.
		DB(config.Main.DBName).
		C(collection).
		Insert(game)
	// Return ID
	return err
}

func JoinDuelGame(hostID, guestID, guestNumber string) (string, error) {
	session := dbSession.Clone()
	defer session.Close()

	err := session.
		DB(config.Main.DBName).
		C(config.DuelGamesCollection).
		Update(bson.M{
			"player_one_data.player_id": hostID,
		}, bson.M{
			"$set": bson.M{
				"player_two_data": bson.M{
					"player_id": guestID,
					"role":      config.RoleGuest,
					"number":    guestNumber,
					"can_guess": true,
				},
			},
		})
	if err != nil {
		log.Error(err)
		return "", err
	}

	game := &models.DuelGame{}
	err = session.
		DB(config.Main.DBName).
		C(config.DuelGamesCollection).
		Find(bson.M{
			"player_one_data.player_id": hostID,
		}).
		Select(bson.M{"public_id": 1, "_id": 0}).
		One(&game)
	// Return ID
	return game.PublicID, err
}

func GetDuelByPublicID(id string) (*models.DuelGame, error) {
	session := dbSession.Clone()
	defer session.Close()
	response := &models.DuelGame{}

	err := session.
		DB(config.Main.DBName).
		C(config.DuelGamesCollection).
		Find(bson.M{"public_id": id}).
		One(response)

	return response, err
}

func GetSoloByPublicID(id string) (*models.SoloGame, error) {
	session := dbSession.Clone()
	defer session.Close()
	response := &models.SoloGame{}

	err := session.
		DB(config.Main.DBName).
		C(config.SoloGamesCollection).
		Find(bson.M{"public_id": id}).
		One(response)

	return response, err
}

func RecordGuess(guess *models.GuessResult, gameID, collection string) error {
	session := dbSession.Clone()
	defer session.Close()
	update := bson.M{
		"$push": bson.M{
			"guesses": guess,
		},
	}

	if guess.Won {
		update["$set"] = bson.M{"won": true}
	}

	err := session.
		DB(config.Main.DBName).
		C(collection).
		Update(bson.M{
			"public_id": gameID,
		}, update)
	return err
}
