package config

const (
	DuelGamesCollection = "duel_games"
	SoloGamesCollection = "solo_games"
	UsersCollection     = "users"
)

const (
	RoleCreator = "creator"
	RoleBot     = "bot"
	RoleGuest   = "guest"
)

const (
	NumberLength = 4
)
