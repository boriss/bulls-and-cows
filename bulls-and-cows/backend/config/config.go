package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sync"

	log "github.com/Sirupsen/logrus"
)

var (
	Main       *Config
	once       sync.Once
	flagConfig = flag.String("config", "", "Configuration file")
)

type Config struct {
	Port        string `json:"port"`
	DBName      string `json:"db_name"`
	UIOriginURL string `json:"ui_origin_url"`
	DBAddress   string `json:"mongo_address"`
	Development bool   `json:"development"`

	// Authentication variables
	UseSecureConnection bool   `json:"use_secure_connection"`
	UIDomain            string `json:"ui_domain"`
	JwtKey              string `json:"jwt_key"`
}

func init() {
	once.Do(func() {
		flag.Usage = usage
		flag.Parse()

		cfgPath := *flagConfig
		Main = GetConfiguration(cfgPath)
	})
}

func usage() {
	fmt.Fprintf(os.Stderr, `Usage: %s --config [config_file] --svcs_config [config_file]`, os.Args[0])
}

// GetConfiguration ...
func GetConfiguration(configPath string) *Config {
	config := new(Config)
	fmt.Println(configPath)
	if len(configPath) > 0 {
		file, err := ioutil.ReadFile(configPath)
		if err != nil {
			log.Panic("Error opening config file!", err)
		}

		if err = json.Unmarshal(file, config); err != nil {
			log.Panic("Error parsing cofiguration file: ", err)
		}
	} else {
		log.Panic("No configuration file provided!")
	}

	return config
}
