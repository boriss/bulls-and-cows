package main

import (
	"fmt"
	"testing"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/models"
)

func TestAIGameReturnsProperBullsAndCows(t *testing.T) {
	// attempts := 0
	creator := models.PlayerData{
		PlayerID: "test-player",
		Number: "1234",
		CanGuess: true,
	}
	game := &models.DuelGame{
		PlayerOneData PlayerData    `json:"player_one_data" bson:"player_one_data"`
		PlayerTwoData PlayerData    `json:"player_two_data" bson:"player_two_data"`
	}
	tests := map[string][]int{
		"1000": []int{1, 0},
		"1234": []int{4, 0},
		"0100": []int{0, 1},
		"1030": []int{2, 0},
		"4321": []int{0, 4},
		"0909": []int{0, 0},
		"1243": []int{2, 2},
		"4567": []int{0, 1},
	}
	vocab := vocabulary{
		WordLength: 4,
		Charset:    []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
	}

	vocab.generate(0, []string{"", "", "", ""})

	for guess, expect := range tests {
		b, c := ComputeMatch(number, guess)
		if b != expect[0] || c != expect[1] {
			t.Errorf(
				"For guess %s with number %s expected %d bulls %d cows, got %d bulls %d cows.",
				guess,
				number,
				expect[0],
				expect[1],
				b,
				c)
		}
	}
}
