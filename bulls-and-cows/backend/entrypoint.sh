#!/bin/bash
set -e
declare run_cmd=$@
if [ "$1" = 'bulls-and-cows' ]; then
    run_cmd+=" --config=$CONFIG_FILE"
fi

exec $run_cmd
