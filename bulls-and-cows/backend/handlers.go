package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/ai"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/data"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/internal/util"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/models"
)

func SoloCreate(w http.ResponseWriter, r *http.Request) {
	// Validate input

	b, err := ioutil.ReadAll(r.Body)
	requestModel := &models.NewSoloGameRequest{}
	err = json.Unmarshal(b, requestModel)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("Error while trying to unmarshal new solo request.")
		util.WriteJSONResponse(w, err, 500)
		return
	}
	defer r.Body.Close()

	game := CreateSolo(requestModel.PlayerID)
	err = data.CreateGame(game, config.SoloGamesCollection)

	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("Error while trying to create new solo.")
		util.WriteJSONResponse(w, err, 500)
		return
	}

	// Success
	util.WriteJSONResponse(w, game.PublicID, 201)
}

func DuelCreate(w http.ResponseWriter, r *http.Request) {
	// Validate input

	b, err := ioutil.ReadAll(r.Body)
	requestModel := &models.NewDuelGameRequest{}
	err = json.Unmarshal(b, requestModel)
	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("Error while trying to unmarshal new duel request.")
		util.WriteJSONResponse(w, err, 500)
		return
	}
	defer r.Body.Close()

	game := CreateVsNPC(requestModel.PlayerNum, requestModel.PlayerID)
	err = data.CreateGame(game, config.DuelGamesCollection)

	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("Error while trying to create new duel.")
		util.WriteJSONResponse(w, err, 500)
		return
	}

	// Success
	// TODO: Add game ID to response
	util.WriteJSONResponse(w, game.PublicID, 201)
}

func DuelJoin(w http.ResponseWriter, r *http.Request) {
	// Validate input

	decoder := json.NewDecoder(r.Body)
	var request models.JoinDuelGameRequest
	err := decoder.Decode(&request)
	if err != nil {
		log.Error(err)
		util.WriteJSONResponse(w, err, 500)
	}
	defer r.Body.Close()
	gameID, err := data.JoinDuelGame(
		request.HostID,
		request.PlayerID,
		request.PlayerNum)

	if err != nil {
		log.Error(err)
		util.WriteJSONResponse(w, err, 500)
	}

	// Success
	util.WriteJSONResponse(w, gameID, 201)
}

func Guess(w http.ResponseWriter, r *http.Request) {
	// Validate input
	gameType := mux.Vars(r)["type"]

	decoder := json.NewDecoder(r.Body)
	var guess *models.Guess
	err := decoder.Decode(&guess)
	log.Info(guess)
	if err != nil {
		log.Error(err)
		util.WriteJSONResponse(w, err, 500)
		return
	}
	defer r.Body.Close()

	var result *models.GuessResult
	switch gameType {
	case "duel":
		result, err = duelGuess(guess)
	case "solo":
		result, err = soloGuess(guess)
	}
	util.WriteJSONResponse(w, result, 200)
}

func duelGuess(guess *models.Guess) (*models.GuessResult, error) {
	game, err := data.GetDuelByPublicID(guess.GameID)

	if err != nil {
		log.Error(err)
		return nil, err
	}

	result := CheckDuelGuess(game, guess)

	err = data.RecordGuess(result, guess.GameID, config.DuelGamesCollection)

	if err != nil {
		log.Error(err)
		return nil, err
	}

	return result, nil
}

func soloGuess(guess *models.Guess) (*models.GuessResult, error) {
	game, err := data.GetSoloByPublicID(guess.GameID)

	if err != nil {
		log.Error(err)
		return nil, err
	}

	result := &models.GuessResult{
		PlayerID: guess.PlayerID,
	}

	result.Bulls, result.Cows = ai.ComputeMatch(game.Number, guess.Number)
	if result.Bulls == config.NumberLength {
		result.Won = true
	}

	err = data.RecordGuess(result, guess.GameID, config.SoloGamesCollection)

	if err != nil {
		log.Error(err)
		return nil, err
	}

	return result, nil
}
