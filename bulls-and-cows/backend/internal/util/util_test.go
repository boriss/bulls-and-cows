package util

import (
	"fmt"
	"testing"
)

func TestRandomNumberGeneration(t *testing.T) {
	for i := 0; i < 10; i++ {
		number := GenerateRandomValidNumber(4)
		fmt.Println("Number: ", number)
	}
	t.Error()
}
