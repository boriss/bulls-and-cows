package util

import (
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"runtime"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
)

const (
	letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
)

type GzipResponseWriter struct {
	io.Writer
	http.ResponseWriter
}

func (w GzipResponseWriter) Write(b []byte) (int, error) {
	return w.Writer.Write(b)
}

func MakeGzipHandler(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if !strings.Contains(r.Header.Get("Accept-Encoding"), "gzip") {
			fn(w, r)
			return
		}
		w.Header().Set("Content-Encoding", "gzip")
		gz := gzip.NewWriter(w)
		defer gz.Close()
		fn(GzipResponseWriter{Writer: gz, ResponseWriter: w}, r)
	}
}

func WriteJSONResponse(w http.ResponseWriter, data interface{}, status int) {
	serialized, serializationErr := json.MarshalIndent(data, "", " ")
	if serializationErr != nil {
		log.Error(serializationErr)
		empty, _ := json.Marshal("Serialization error")
		serialized = empty
	}

	contentType := "application/json; charset=utf-8"

	w.Header().Set("Content-Type", contentType)
	w.WriteHeader(status)
	log.Debug(string(serialized))
	w.Write(serialized)
}

func GetFileInfo() string {
	_, file, line, _ := runtime.Caller(1)
	fileNames := strings.Split(file, "/")
	f := fileNames[len(fileNames)-1]
	return fmt.Sprintf("%v:%v", f, line)
}

func RandString(n int) string {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[r.Int63()%int64(len(letters))]
	}
	return string(b)
}

func GenerateRandomValidNumber(length int) string {
	digits := "123456789"
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	// Append digit that is not 0
	number := string(digits[r.Intn(len(digits))])
	digits = strings.Replace(digits, number, "", 1)
	// Append 0 to the list of digits
	digits += "0"
	for i := 0; i < length-1; i++ {
		digit := string(digits[r.Intn(len(digits))])
		number += digit
		digits = strings.Replace(digits, digit, "", 1)
		log.Info(digits)
	}
	return number
}
