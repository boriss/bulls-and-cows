package auth

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/data"

	log "github.com/Sirupsen/logrus"
	"github.com/dgrijalva/jwt-go"
	"google.golang.org/api/oauth2/v2"
)

type (
	AuthHandler struct {
		JwtKey string
	}
)

func init() {
	// cfg := config.GetConfiguration()
}

func (a AuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	log.Info("Authorizing")

	// Validate the JWT token if one exists.
	authCookie, err := r.Cookie("Auth")

	if err != nil {
		handleUnauthorized(w, r)
		return
	}
	authToken := authCookie.Value
	token, err := jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(a.JwtKey), nil
	})

	if err != nil {
		log.WithFields(log.Fields{"error": err}).Error("Error parsing jwt token for auth.")
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		email := claims["email"].(string)
		if checkIfEmaiIsRegistered(email) {
			// Happy path
			log.Info("Authorized")
			next(w, r)
			return
		}
		handleUnauthorized(w, r)
		return
	}
	handleUnauthorized(w, r)
	return
}

func handleUnauthorized(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusUnauthorized)
	w.Write([]byte("Unauthorized."))
}

func checkIfEmaiIsRegistered(email string) bool {
	user, err := data.GetUserByEmail(email)
	if user == nil || err != nil {
		log.Error(err)
		return false
	}
	return true
}

func FromCSRFHeader(param string) func(r *http.Request) (string, error) {
	return func(r *http.Request) (string, error) {
		token := ""
		xCsrfTokenCookie, err := r.Cookie("X-Csrf-Token")
		if err == nil {
			token = xCsrfTokenCookie.Value
		}

		// No error, just no token
		return token, nil
	}
}

type loginBody struct {
	Name string `json:"name"`
}

func Signin(w http.ResponseWriter, r *http.Request) {
	log.Info("Logging in")
	// Request the profile info from Google
	authHeader := strings.Replace(r.Header.Get("Authorization"), "Bearer ", "", 1)

	decoder := json.NewDecoder(r.Body)
	requestData := &loginBody{}
	err := decoder.Decode(&requestData)
	if err != nil {
		log.Error(err)
		handleUnauthorized(w, r)
	}
	defer r.Body.Close()

	httpClient := &http.Client{}
	oauth2Service, err := oauth2.New(httpClient)
	tokenInfoCall := oauth2Service.Tokeninfo()
	tokenInfoCall.IdToken(authHeader)
	tokenInfo, err := tokenInfoCall.Do()
	if err != nil {
		log.Error(err)
		handleUnauthorized(w, r)
		return
	}

	if checkIfEmaiIsRegistered(tokenInfo.Email) {
		handleLogin(w, tokenInfo.Email)
	} else {
		err = data.CreateUser(tokenInfo.Email, requestData.Name)
		if err != nil {
			handleUnauthorized(w, r)
		}
		handleLogin(w, tokenInfo.Email)
	}

	return
}

func handleLogin(w http.ResponseWriter, email string) {
	cfg := config.Main
	authToken := jwt.New(jwt.SigningMethodHS256)

	claims := authToken.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()
	claims["email"] = email

	authTokenString, _ := authToken.SignedString([]byte(cfg.JwtKey))
	authCookie := &http.Cookie{
		Name:     "Auth",
		Value:    authTokenString,
		Expires:  time.Now().Add(time.Hour * 24 * 7),
		Path:     "/",
		Secure:   cfg.UseSecureConnection,
		Domain:   cfg.UIDomain,
		HttpOnly: true,
	}
	http.SetCookie(w, authCookie)

	csrfToken := jwt.New(jwt.SigningMethodHS256)

	csrfClaims := csrfToken.Claims.(jwt.MapClaims)
	csrfClaims["exp"] = time.Now().Add(time.Hour * 24 * 7).Unix()
	csrfTokenString, _ := csrfToken.SignedString([]byte(cfg.JwtKey))

	csrfCookie := &http.Cookie{
		Name:     "X-Csrf-Token",
		Value:    csrfTokenString,
		Expires:  time.Now().Add(time.Hour * 24 * 7),
		Path:     "/",
		Secure:   cfg.UseSecureConnection,
		Domain:   cfg.UIDomain,
		HttpOnly: false,
	}
	http.SetCookie(w, csrfCookie)
	log.Info("Signed in")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte{})
}

func Signout(w http.ResponseWriter, r *http.Request) {
	log.Info("SIGNED OUT")
	cfg := config.Main
	authCookie := &http.Cookie{
		Name:     "Auth",
		Value:    "deleted",
		Expires:  time.Unix(0, 0),
		Path:     "/",
		Secure:   cfg.UseSecureConnection,
		Domain:   cfg.UIDomain,
		HttpOnly: true,
	}
	http.SetCookie(w, authCookie)
	csrfCookie := &http.Cookie{
		Name:     "X-Csrf-Token",
		Value:    "deleted",
		Expires:  time.Unix(0, 0),
		Path:     "/",
		Secure:   cfg.UseSecureConnection,
		Domain:   cfg.UIDomain,
		HttpOnly: false,
	}
	http.SetCookie(w, csrfCookie)

	log.Info("Signed out")
	log.Info(w.Header())
	w.WriteHeader(http.StatusOK)
	w.Write([]byte{})
	return
}
