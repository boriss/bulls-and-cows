package infrastructure

import (
	"net/http"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func InitLogging() {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: "2006-01-02T15:04:05.999999-07:00",
	})

	// Only log the info severity or above.
	log.SetLevel(log.InfoLevel)

	fatalHandler := func() {
		log.Info("Shutting down.")
	}
	log.RegisterExitHandler(fatalHandler)
}

// func getAppHealthStatus(w http.ResponseWriter, r *http.Request) {
//     session := config.DBSession.Clone()
//     defer session.Close()
//     err := session.Ping()
//
//     if err != nil {
//         log.Fatal(err)
//     } else {
//         w.WriteHeader(http.StatusOK)
//     }
// }

func SetupInfrastructureServer() {
	router := mux.NewRouter().StrictSlash(true)
	// router.HandleFunc("/api/v1/infrastructure/health-check", getAppHealthStatus).Methods("GET")
	router.Handle("/api/v1/infrastructure/metrics", promhttp.Handler()).Methods("GET")
	server := &http.Server{
		Addr:        "0.0.0.0:6060",
		Handler:     router,
		ReadTimeout: 30 * time.Second, // to prevent abuse of "keep-alive" requests by clients
	}
	log.Info(server.ListenAndServe())
}
