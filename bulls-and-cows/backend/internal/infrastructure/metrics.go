package infrastructure

import (
	"github.com/urfave/negroni"
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	buckets = []float64{300, 1200, 5000}
)

const (
	requestsCounterName = "admin_api_requests_total"
	requestsCounterHelp = "How many HTTP requests processed, partitioned by status code, method and HTTP path."
	requestsLatencyName = "admin_api_request_duration_milliseconds"
	requestsLatencyHelp = "How long it took to process a request, partitioned by status code, method and HTTP path."
)

type PrometheusMiddleware struct {
	requests *prometheus.CounterVec
	latency  *prometheus.HistogramVec
}

func NewPrometheusMiddleware(name string) *PrometheusMiddleware {
	var m PrometheusMiddleware
	m.requests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name:        requestsCounterName,
			Help:        requestsCounterHelp,
			ConstLabels: prometheus.Labels{"service": name},
		},
		[]string{"code", "method", "path"},
	)
	prometheus.MustRegister(m.requests)

	m.latency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:        requestsLatencyName,
		Help:        requestsLatencyHelp,
		ConstLabels: prometheus.Labels{"service": name},
		Buckets:     buckets,
	},
		[]string{"code", "method", "path"},
	)
	prometheus.MustRegister(m.latency)
	return &m
}

func (m *PrometheusMiddleware) ServeHTTP(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	start := time.Now()
	res := negroni.NewResponseWriter(rw)
	next(res, r)
	m.requests.WithLabelValues(http.StatusText(res.Status()), r.Method, r.URL.Path).Inc()
	m.latency.WithLabelValues(http.StatusText(res.Status()), r.Method, r.URL.Path).Observe(float64(time.Since(start).Nanoseconds()) / 1000000)
}
