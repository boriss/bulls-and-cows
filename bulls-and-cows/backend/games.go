package main

import (
	"errors"
	"time"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/ai"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/internal/util"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/models"
)

func CreateSolo(id string) *models.SoloGame {
	number := util.GenerateRandomValidNumber(config.NumberLength)
	now := time.Now().UTC().Unix()
	publicID := util.RandString(7)

	result := &models.SoloGame{
		PublicID:  publicID,
		PlayerID:  id,
		Number:    number,
		State:     "created",
		Timestamp: now,
	}
	return result
}

func CreateVsNPC(userNum, id string) *models.DuelGame {
	botNumber := util.GenerateRandomValidNumber(config.NumberLength)
	now := time.Now().UTC().Unix()
	publicID := util.RandString(7)

	result := &models.DuelGame{
		PublicID: publicID,
		State:    "started",
		PlayerOneData: models.PlayerData{
			PlayerID: id,
			Role:     config.RoleCreator,
			Number:   userNum,
			CanGuess: true,
		},
		PlayerTwoData: models.PlayerData{
			Role:     config.RoleBot,
			Number:   botNumber,
			CanGuess: false,
		},
		Timestamp: now,
	}

	return result
}

func CheckDuelGuess(game *models.DuelGame, guess *models.Guess) *models.GuessResult {
	var opponent models.PlayerData
	result := &models.GuessResult{
		PlayerID: guess.PlayerID,
	}

	if game.PlayerOneData.PlayerID == guess.PlayerID {
		opponent = game.PlayerTwoData
	} else if game.PlayerOneData.PlayerID == guess.PlayerID {
		opponent = game.PlayerOneData
	} else {
		result.Error = errors.New("Player is not part of this game")
	}

	result.Bulls, result.Cows = ai.ComputeMatch(opponent.Number, guess.Number)
	if result.Bulls == config.NumberLength {
		result.Won = true
	}

	return result
}
