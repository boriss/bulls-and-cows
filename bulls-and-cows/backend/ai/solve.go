package ai

import (
	"math/rand"
	"strings"
	"time"
)

func ComputeMatch(oldWord, newWord string) (int, int) {
	matches := map[string]int{}
	var bulls, cows int
	for i := 0; i < len(oldWord); i++ {
		oldChar := string(oldWord[i])
		newChar := string(newWord[i])
		if oldChar == newChar {
			bulls++
		} else {
			if match, ok := matches[oldChar]; ok {
				if match < 0 {
					cows++
				}
				matches[oldChar]++
			} else {
				matches[oldChar] = 1
			}

			if match, ok := matches[newChar]; ok {
				if match > 0 {
					cows++
				}
				matches[newChar]--
			} else {
				matches[newChar] = -1
			}
		}
	}
	return bulls, cows
}

type bot struct {
	WordLength int
	Vocabulary []string
	Charset    []string
}

func (b *bot) generate(idx int, word []string) {
	if idx >= b.WordLength {
		b.Vocabulary = append(b.Vocabulary, strings.Join(word, ""))
	} else {
		for _, c := range b.Charset {
			if idx == 0 && c == "0" {
				continue
			}

			isFirst := true
			for i := 0; i < idx; i++ {
				if c == string(word[i]) {
					isFirst = false
					break
				}
			}
			if !isFirst {
				continue
			}

			word[idx] = c
			b.generate(idx+1, word)
		}
	}
}

func (b *bot) PruneVocab(word string, bulls, cows int) {
	newVocabulary := []string{}
	var currentBulls, currentCows int
	for i, w := range b.Vocabulary {
		currentBulls, currentCows = ComputeMatch(word, w)
		if currentBulls == bulls && currentCows == cows {
			newVocabulary = append(newVocabulary, b.Vocabulary[i])
		}
	}
	b.Vocabulary = newVocabulary
}

func (b *bot) RandomGuess() string {
	index := int(rand.
		New(rand.NewSource(time.Now().UnixNano())).
		Int63n(int64(len(b.Vocabulary))))
	return b.Vocabulary[index]
}
