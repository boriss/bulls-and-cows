package ai

import (
	"fmt"
	"testing"
)

func TestAIGameReturnsProperBullsAndCows(t *testing.T) {
	// attempts := 0
	number := "1234"
	tests := map[string][]int{
		"1000": []int{1, 0},
		"1234": []int{4, 0},
		"0100": []int{0, 1},
		"1030": []int{2, 0},
		"4321": []int{0, 4},
		"0909": []int{0, 0},
		"1243": []int{2, 2},
		"4567": []int{0, 1},
	}
	b := bot{
		WordLength: 4,
		Charset:    []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
	}

	b.generate(0, []string{"", "", "", ""})

	for guess, expect := range tests {
		b, c := ComputeMatch(number, guess)
		if b != expect[0] || c != expect[1] {
			t.Errorf(
				"For guess %s with number %s expected %d bulls %d cows, got %d bulls %d cows.",
				guess,
				number,
				expect[0],
				expect[1],
				b,
				c)
		}
	}
}

func AIFullTest(t *testing.T) {
	// attempts := 0
	number := "1234"
	b := bot{
		WordLength: 4,
		Charset:    []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"},
	}

	b.generate(0, []string{"", "", "", ""})

	// var b, c int
	// var guess string
	// for b < 4 && len(b.Vocabulary) >= 2 {
	//     guess = RandomGuess()
	bulls, cows := ComputeMatch(number, "3000")
	fmt.Println(bulls, cows)
	//     fmt.Println(guess)
	//     fmt.Println(b, " ", c)
	//     b.PruneVocab(guess, b, c)
	//     attempts++
	// }
	// fmt.Println(attempts, " ", guess, " ", number, " ", b, " ", len(b.Vocabulary))
	t.Error()
}
