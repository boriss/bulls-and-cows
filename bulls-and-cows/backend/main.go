package main

import (
	stdlog "log"
	"net/http"
	"os"
	"runtime"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	// "github.com/phyber/negroni-gzip/gzip"
	"github.com/rs/cors"
	"github.com/unrolled/secure"
	"github.com/urfave/negroni"
	"gopkg.in/tylerb/graceful.v1"

	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/config"
	"tig.chaosgroup.com/boris.stoyanovv/bulls-and-cows/backend/internal/auth"
)

var logger *stdlog.Logger

func initLogging() {
	logger = stdlog.New(os.Stdout, "[bulls-and-cows-api:] ", 0)

	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: "2006-01-02T15:04:05.999999-07:00",
	})
	log.SetLevel(log.InfoLevel)
}

func init() {
	initLogging()
}

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	routerMain := mux.NewRouter().StrictSlash(true)

	// Setup login subrouter
	loginRouter := mux.NewRouter().PathPrefix("/auth").Subrouter().StrictSlash(true)
	loginRouter.HandleFunc("/signin", auth.Signin).Methods("POST")
	// loginRouter.HandleFunc("/sign-out", auth.Signout).Methods("POST")

	// Setup API subrouter
	routerAPI := mux.NewRouter().PathPrefix("/api").Subrouter().StrictSlash(true)

	routerAPI.HandleFunc("/duel/npc/new", DuelCreate).Methods("POST")
	routerAPI.HandleFunc("/solo/new", SoloCreate).Methods("POST")
	routerAPI.HandleFunc("/duel/join", DuelJoin).Methods("POST")
	routerAPI.HandleFunc("/{type}/guess", Guess).Methods("POST")

	secureMiddleware := secure.New(secure.Options{
		FrameDeny:             true,
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		IsDevelopment:         config.Main.Development,
		ContentSecurityPolicy: "default-src 'self'",
		PublicKey:             `pin-sha256="base64+primary=="; pin-sha256="base64+backup=="; max-age=5184000; includeSubdomains;"`,
	})
	if secureMiddleware != nil {
	}

	// Set CORS headers
	c := cors.New(cors.Options{
		AllowedOrigins: []string{config.Main.UIOriginURL},
		AllowedHeaders: []string{
			"Authorization",
			"Content-Type",
			"X-Csrf-Token",
			"Auth",
			"Access-Control-Allow-Origin",
			"Accept",
			"Accept-Encoding",
			"Accept-Language",
		},
		AllowCredentials: true,
	})

	// Setup JWT authentication
	jwtCSRFMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(config.Main.JwtKey), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
		Extractor:     auth.FromCSRFHeader("X-Csrf-Token"),
	})

	// Setup auth
	authHandler := auth.AuthHandler{
		JwtKey: config.Main.JwtKey,
	}

	routerMain.PathPrefix("/auth").Handler(negroni.New(
		negroni.HandlerFunc(secureMiddleware.HandlerFuncWithNext),
		// gzip.Gzip(gzip.DefaultCompression),
		negroni.Wrap(loginRouter),
	))

	routerMain.PathPrefix("/api").Handler(negroni.New(
		negroni.HandlerFunc(jwtCSRFMiddleware.HandlerWithNext),
		negroni.HandlerFunc(secureMiddleware.HandlerFuncWithNext),
		// gzip.Gzip(gzip.DefaultCompression),
		negroni.HandlerFunc(authHandler.ServeHTTP),
		negroni.Wrap(routerAPI),
	))

	n := negroni.New()

	// prometheusMiddleware := infrastructure.NewPrometheusMiddleware("bulls-and-cows")
	// n.Use(prometheusMiddleware)
	n.Use(negroni.NewRecovery())
	n.Use(&negroni.Logger{logger})
	n.Use(c)
	n.UseHandler(routerMain)

	// Setup graceful shutdown of server
	server := &graceful.Server{
		Timeout: 10 * time.Second,
		Server: &http.Server{
			Addr:        "0.0.0.0:" + config.Main.Port,
			Handler:     n,
			ReadTimeout: 30 * time.Second, // to prevent abuse of "keep-alive" requests by clients
		},
	}

	log.Info("Server listening on port ", config.Main.Port)
	log.Fatal(server.ListenAndServe())

	log.Info("Done.")
}
