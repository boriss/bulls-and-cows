package models

type (
	NewSoloGameRequest struct {
		PlayerID string `json:"player_id"`
	}

	NewDuelGameRequest struct {
		PlayerID  string `json:"player_id"`
		PlayerNum string `json:"player_num"`
	}

	JoinDuelGameRequest struct {
		PlayerID  string `json:"player_id"`
		PlayerNum string `json:"player_num"`
		HostID    string `json:"host_id"`
	}

	SoloGame struct {
		PublicID  string        `json:"public_id" bson:"public_id"`
		PlayerID  string        `json:"player_id" bson:"player_id"`
		Number    string        `json:"number" bson:"number"`
		State     string        `json:"state" bson:"state"`
		Timestamp int64         `json:"timestamp" bson:"timestamp"`
		Guessess  []GuessResult `json:"guesses" bson:"guesses"`
	}

	DuelGame struct {
		PublicID      string        `json:"public_id" bson:"public_id"`
		PlayerOneData PlayerData    `json:"player_one_data" bson:"player_one_data"`
		PlayerTwoData PlayerData    `json:"player_two_data" bson:"player_two_data"`
		State         string        `json:"state" bson:"state"`
		WinnerID      string        `json:"winner_id" bson:"winner_id"`
		Timestamp     int64         `json:"timestamp" bson:"timestamp"`
		Guessess      []GuessResult `json:"guesses" bson:"guesses"`
	}

	PlayerData struct {
		PlayerID string `json:"player_id,omitempty" bson:"player_id,omitempty"`
		Role     string `json:"role" bson:"role"`
		Number   string `json:"number" bson:"number"`
		CanGuess bool   `json:"can_guess" bson:"can_guess"`
	}

	Guess struct {
		GameID   string `json:"game_id" bson:"game_id"`
		PlayerID string `json:"player_id" bson:"player_id"`
		Number   string `json:"number" bson:"number"`
	}

	GuessResult struct {
		PlayerID string `json:"-" bson:"player_id"`
		Bulls    int    `json:"bulls" bson:"bulls"`
		Cows     int    `json:"cows" bson:"cows"`
		Error    error  `json:"error,omitempty" bson:"error,omitempty"`
		Won      bool   `json:"won,omitempty" bson:"won,omitempty"`
	}

	User struct {
		Email string `json:"email" bson:"email"`
		Name  string `json:"name" bson:"name"`
	}
)
