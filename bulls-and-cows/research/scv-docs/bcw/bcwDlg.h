// bcwDlg.h : header file
//
/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

#if !defined(AFX_BCWDLG_H__18284512_8889_498B_91EB_BBB3076E8039__INCLUDED_)
#define AFX_BCWDLG_H__18284512_8889_498B_91EB_BBB3076E8039__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBcwDlg dialog
#include "treeNode.h"

class CBcwDlg : public CDialog{
// Construction
public:

	/*BULLS_COWS & MASTERMIND creates js file, (for all algorithms)
	to create .js file with all trees at first #define _BULLS_COWS_ and call createJavaScriptStringTreeAll()
	then comment #define _BULLS_COWS_ and call createJavaScriptStringTreeAll() again
	*/
	void createJavaScriptStringTreeAll();

	//BULLS_COWS & MASTERMIND
	int findEnumResponse(int r);

	//BULLS_COWS & MASTERMIND creates js file, (for particular algorithm)
	void createJavaScriptStringTree(const char TREE_FILE[],const bool clearJsFile);

	int countInAndCheckSum(TreeNode*t,CPtrTurnType gr,const int cgr,const bool STORE_SUM_TURNS);

	static void codeDigitJavaScript(char*c,int n);
	static void codeNumberJavaScript(char*c,int n,bool oneDigit);
	static bool checkFileExists(const char* fName);

	/*
		create string for algorithm crush45,crush5 for file tree_string.h
		this function is not used for Mastermind because of fast algorithm
	*/
	void createStringForCrush();

	void test();

	/*
	BULLS_COWS & MASTERMIND
	store tree 

	for BULLS_COWS
	alg=5 crush5  | avg5
	alg=6 from set only

	for MASTERMIND
	alg=4 = avg(max 5 turns) | crushExact
	alg=5 = avg(max 6 turns)

	NOTE after algorithm modifiing, new trees can not be constructed for crush algorithms, cause one need to
	do computations for exact6,exact5,exact4.
	*/
	void storeTree(const int _alg,const bool allowFromSetOnly);

	CString startTimeS,endTimeS;
	void setTimeString(bool p);

	/*
	BULLS_COWS & MASTERMIND
	solving cycle by mde (same with subgroups but with last response)  
	int t[]={  123, 1435 };
	int r[]={ BC03, BC01 };
	*/
	void solveSubgroupsResponse(const bool allowFromSetOnly);


	/*
	BULLS_COWS & MASTERMIND
	solve all subgroups 
		int t[]={     123,1456 };
		int r[]={ BC(0,1),  ** };

	#ifdef CYCLE_TURN - function generates jobs for exact algorithm step 1-5
	*/
	void solveSubgroups(const bool allowFromSetOnly);

	/*
	BULLS_COWS & MASTERMIND
	store equvalent turns classes
	store possible turns for turn2-3 for bulls and cows game and turn1-2 for mastermind
	*/
	void storeTurns();

	/*
	helper functions
	*/
	static const CString formatNumber(int);
	static CString getAlgorithmString(int mde,int count,int length);
	static const CString getHeuristicAlgorithmName();
	static const CString getShortAlgorithmName(bool capitilizeFirstChar=false);
	static void appendCharsToLength(CString&s,const int length,const char add=' ');

	void solveAll();

	/*
	supports BULLS_COWS & MASTERMIND
	*/
	void showSubgroups();

	//count subgroups
	void countNumbers();

	bool canClose;
	bool mbCheck();
	void proceedFile(const char* fName,int& e,int& t,int& nth,bool thirdTurn=true);
	CBcwDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBcwDlg)
	enum { IDD = IDD_BCW_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBcwDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon; 
	// Generated message map functions
	//{{AFX_MSG(CBcwDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnButtonStore();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCWDLG_H__18284512_8889_498B_91EB_BBB3076E8039__INCLUDED_)
