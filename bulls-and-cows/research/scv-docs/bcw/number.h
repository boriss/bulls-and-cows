/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#ifndef _number_
#define _number_

#include "turns.h"

#define RESPONSE(a,b) Number::RA[(a)*MAXN+(b)]

#ifdef _ALLOW_SAME_DIGITS_
	#define DIFFERENT_TURN1 TURN1
	#define DIFFERENT_TURN2 TURN2
#else
	#define DIFFERENT_TURN1 TURN2
	#define DIFFERENT_TURN2 TURN3
#endif

class Number{
	char n[DIGITS];

	static int arrayIndex(CPtrTurnType array,int value);

public:
	Number(){
	};

	static Number array[MAXN];

	static char*RA;

	inline char getN(const int i)const{
		return n[i];
	}

  //response A on move X getResponse(X,A)=getResponse(A,X) for any  A & X
  int	getResponse(Number const x)const;

  inline int Int()const{
    return n[0]*1000+n[1]*100+n[2]*10+n[3];
  }
	
	static void init();
	static void deInit();
	static int findIndex(int n);

	//use for helper.cpp from console project
	//return true if number has only digits 0-3
	bool onlyDigits0123()const;

	static inline int Number::firstDifferentTurnIndex(int turn){
		return arrayIndex(DIFFERENT_TURN1,findIndex(turn));
	}

	static inline int secondDifferentTurnIndex(int firstTurnIndex,int turn){
		return arrayIndex(DIFFERENT_TURN2[firstTurnIndex],findIndex(turn));
	}

	static CPtrTurnType firstDifferentTurnPtr(int turn);

	/*
	[for bulls cows] returns &(TURN3[t2i][x]) : TURN3[t2i][x]=v 
	[for mastermind] returns &(TURN2[t2i][x]) : TURN2[t2i][x]=v 
	*/
	static CPtrTurnType secondDifferentTurnPtr(int firstTurnIndex,int turn);


};

#endif
