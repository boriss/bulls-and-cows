/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

if(alpha>=beta || beta<=0){
	return beta;
}

if(depth > sNCount+1){
	return MAXE;
}

if(cgr==1){
	best=gr[0];
	#ifdef  _MINIMIZE_AVERAGE_
	return depth;
	#else
	return depth==sNCount+1?1:0;
	#endif
}

if(depth == sNCount+1){
	return MAXE;
}

if(cgr==2){
	best=gr[0];
	#ifdef  _MINIMIZE_AVERAGE_
	return 2*depth+1;
	#else
	return depth==sNCount?1:0;
	#endif
}
else if(cgr==3){
	#ifdef  _MINIMIZE_AVERAGE_
	e=3*depth+2;
	#else
	e=depth==sNCount?2:0;
	#endif

	#define m(x,y1,y2) if(RESPONSE(gr[x],gr[y1])!=RESPONSE(gr[x],gr[y2])){best=gr[x];return e;}
	m(0,1,2);
	m(1,0,2);
	m(2,0,1);
	#undef m
}

if(depth==sNCount){
	return solveLast(alpha,beta,gr,cgr,allowFromSetOnly);
}

if(depth==sNCount-1 && cgr>1+szEnumResponse*(szEnumResponse-1)){
	return MAXE;
}

#if defined(_BULLS_COWS_) && !defined(_MINIMIZE_AVERAGE_)
if(sNCount==6){//
	if(depth+cgr<=7){
		best=gr[0];
		return 0;
	}
	if(!allowFromSetOnly){//DONT remove need to crush5o got estimate=50
		i=solve(alpha,beta,gr,cgr,true);
		if(i<beta){
			beta=i;
			if(beta==0){
				return 0;
			}
		}
	}
}
#endif


//#ifdef _SOLVE_  include from solve otherwise from solveHeuristic
#ifdef _SOLVE_ 
if(depth>sDepthExact){
	t=best;
	i=solveHeuristic(alpha,beta,gr,cgr,allowFromSetOnly);
	if(i>=beta){
		best=t;
	}
	return i;

}
#endif


//set pt
setIn(gr,cgr);

#ifdef _ALLOW_SAME_DIGITS_
	if(depth==1){
		turnCount=0;
		pt=TURN1;
	}
	else if(depth==2){
		pt=TURN2[prev->turnCount];
	}
#else
	if(depth==2){
		turnCount=0;
		pt=TURN2;
	}
	else if(depth==3){
		pt=TURN3[prev->turnCount];
	}
#endif

else{
	if(allowFromSetOnly ){
		gr[cgr]=-1;
		pt=gr;
	}
	else{
		pt=NULL;
		pt=reorderTurns(pt,gr,cgr);
	}
}
