/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net

created 1.jun.2014
*****************************************************************************************************************/

#ifndef numbers_group
#define numbers_group

class NumbersGroup{
public:
	PtrTurnType number;//numbers in group
	short count;//amount of numbers group

	inline NumbersGroup(){
		number=NULL;
	}

	inline void create(CTurnType data[MAXN],int size){
		count=size;
		free();
		number=new TurnType[size];
		memcpy(number,data,count*sizeof(TurnType));
	}

	inline free(){
		if(number!=NULL){
			delete[]number;
			number=NULL;
		}
	}

	inline ~NumbersGroup(){
		free();
	}

	static inline bool compareNumbersGroup(const NumbersGroup* p1,const NumbersGroup* p2){
		return p1->number[0]<p2->number[0];
	}

};

const unsigned GROUPSSET_HASH_BITS=10;
const unsigned GROUPSSET_HASH_SIZE=1<<GROUPSSET_HASH_BITS;//should be power of 2

class GroupsSet{
	union{
		unsigned short code;
		struct{
			unsigned count:4;
			unsigned split:11;//groups with size 1<=13,groups with size 2|3<=12 total 13*12*12=1872<2^11
			unsigned in:1;
		};
	};

	NumbersGroup group[szEnumResponse-1];
	NumbersGroup* vgroup[szEnumResponse-1];
public:
	GroupsSet*next;
	void init(CTurnType sp[MBC][MAXN],const int cnt[MBC],bool _in);//sp need to be a 'TurnType' type because of memcpy function
	bool operator==(GroupsSet const& s)const;
	unsigned hash()const;

	void print(FILE*f,int _number)const;
};

typedef GroupsSet* PtrGroupsSet;


#endif