/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#include "StdAfx.h"
#include "transposition.h"
#include "number.h"
#include "Permutations.h"

Transposition* Transposition::POSITION_TRANSFORM;
int Transposition::POSITION_TRANSFORM_SIZE;

void Transposition::init(){
	Permutations permutation(POSITIONS,POSITIONS,PERMUTATIONS_WITHOUT_REPLACEMENTS);
	POSITION_TRANSFORM_SIZE=permutation.number();
	POSITION_TRANSFORM=new Transposition[POSITION_TRANSFORM_SIZE];
	
	int i;
	Transposition*p=POSITION_TRANSFORM;
	do{
		for(i=0;i<permutation.getK();i++){
			p->p[i]=permutation.getIndex(i);
		}
		p++;
	}while(permutation.next());

}

void Transposition::deInit(){
	delete[]POSITION_TRANSFORM;
}

void Transposition::getIndex(int index,int*iv,int*niv)const{
	int i;
	Number v=Number::array[index];
	for(i=0;i<POSITIONS;i++){
		iv[i]=v.getN(i);
	}

	for(i=0;i<POSITIONS;i++){
		niv[i]=iv[p[i]];
	}
}

bool Transposition::transpositionExists(int*o,int*n){
	int i,*a;
	for(i=0;i<POSITIONS;i++){
		a=d+o[i];

		if(*a==-1){
			if(id[n[i]]){
				return false;
			}
			else{
				*a=n[i];
				id[n[i]]=true;
			}
		}
		else{
			if(*a!=n[i]){
				return false;
			}
		}
	}
	return true;
}

bool Transposition::isEquivalent(int const* t,int tc,int p,int q){
	for(int i=0;i<POSITION_TRANSFORM_SIZE;i++){
		if(POSITION_TRANSFORM[i].leaveSame(t,tc,p,q)){
			return true;
		}
	}
	return false;

}

bool Transposition::leaveSame(int const* t,int tc,int p,int q){
	int i;

	for(i=0;i<DIGITS;i++){
		d[i]=-1;
		id[i]=false;
	}

	int o[POSITIONS],n[POSITIONS];
	for(i=0;i<tc;i++){
		getIndex(t[i],o,n);
		if(!transpositionExists(o,n) ){
			return false;
		}
	}

	//q->p
	int oq[POSITIONS],nq[POSITIONS];
	getIndex(p,o,n);
	getIndex(q,oq,nq);
	
	return transpositionExists(oq,n);

}