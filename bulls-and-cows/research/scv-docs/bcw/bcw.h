// bcw.h : main header file for the BCW application
//
/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

#if !defined(AFX_BCW_H__EC56CF66_E2B8_4FB8_9199_AC648D503A37__INCLUDED_)
#define AFX_BCW_H__EC56CF66_E2B8_4FB8_9199_AC648D503A37__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CBcwApp:
// See bcw.cpp for the implementation of this class
//

class CBcwApp : public CWinApp
{
public:
	CBcwApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBcwApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CBcwApp)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCW_H__EC56CF66_E2B8_4FB8_9199_AC648D503A37__INCLUDED_)
