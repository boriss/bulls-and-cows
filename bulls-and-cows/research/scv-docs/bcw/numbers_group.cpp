/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net

created 1.jun.2014
*****************************************************************************************************************/


#include "stdafx.h"
#include "numbers_group.h"
#include "number.h"

const int add[]={1,13,13*12};

void GroupsSet::init(CTurnType sp[MBC][MAXN],const int cnt[MBC],bool _in){//sp need to be a 'CTurnType' type because of memcpy function
	int i,k;

	code=0;
	in=_in?1:0;

	NumbersGroup*pn=group;

#define m(x,y1,y2) RESPONSE(sp[i][x],sp[i][y1])!=RESPONSE(sp[i][x],sp[i][y2])

	for(count=i=0;i<MBC;i++){
		k=cnt[i];
		if(k==0){
			continue;
		}
		else if(k<3 || k==3 && (m(0,1,2) 	|| m(1,0,2) || m(2,0,1)	) ){
			split+=add[k-1];
		}
		else{
			pn->create(sp[i],k);
			vgroup[count++]=pn;
			pn++;
		}
	}
	std::sort(vgroup,vgroup+count,NumbersGroup::compareNumbersGroup);
#undef m
}

bool GroupsSet::operator==(GroupsSet const& s)const{
	if(code!=s.code){
		return false;
	}

	int i;
	NumbersGroup *const* it;
	NumbersGroup *const* is;
	for( i=0,is=s.vgroup,it=vgroup ; i<count ; it++,is++,i++){
		if((*it)->count!=(*is)->count){
			return false;
		}
	}

	for(i=0,is=s.vgroup,it=vgroup;i<count;it++,is++,i++){
		if(memcmp((*it)->number, (*is)->number, (*it)->count*sizeof(TurnType))!=0){
			return false;
		}
	}
	return true;

}

unsigned GroupsSet::hash()const{
	unsigned c=code^(code>>GROUPSSET_HASH_BITS);

	int i;
	NumbersGroup *const* it;
	for( i=0,it=vgroup ; i<count ; it++,i++){
		c^=(*it)->count<<(i>>2);
	}
	return c&(GROUPSSET_HASH_SIZE-1);
}

void GroupsSet::print(FILE*f,int _number)const{
	int i,j;
	fprintf(f,"%04d %s",_number,in?"in":"out");
	NumbersGroup *const* it;
	for(i=0,it=vgroup;i<count;it++,i++){
		for(j=0;j<(*it)->count;j++){
			fprintf(f,"%c%d",j==0?'{':',',(*it)->number[j]);
		}
		fprintf(f,"}");
	}
	for(i=0;i<3;i++){
		j=split/add[i];
		if(i+1!=3){
			j%=add[i+1];
		}
		if(j!=0){
			fprintf(f,",%d*%d",i+1,j);
		}
	}
	fprintf(f,"\n");
}
