/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#ifndef _solver_
#define _solver_

#include "numbers_group.h"
#include "hash.h"

const int MIN_TURN_NUMBER= ALLOW_SAME_DIGITS ? 0 :1;

#define COUNT_NODES

#ifdef USE_HASH
	#define COUNT_HASH_NODES
#endif

class Solver{
	static Solver p[8];//HAVE TO USE SIZE 8, cause sometimes need to call to make 7th turh, & p[i].depth=i FIXED 3aug2011
	static int turn[7];//turns

	static int sNCount;//=6 count 7th,=5 count 6th...
	static int sOutDepth;
	static int sDepthExact;//solve exact if depth>sDepthExact (mde Max Depth Exact)
	static clock_t sTimeStart;

#ifndef _CONSOLE
	static CWnd* sOut[5];
#endif
	int turnCount;//store turn used for depth=2 & depth=3
	int best;
	TurnType q[MBC][MAXN];//1jun2014 have to be 'TurnType' same type with NumbersGroup 'number' member type
	int cq[MBC];

	GroupsSet *split;
	PtrGroupsSet hashSplit[GROUPSSET_HASH_SIZE];

	char in[MAXN];
	TurnType orderedTurns[MAXN+ALLOW_SAME_DIGITS];

	int depth;//number of turn, 3=make 3rd turn...
	Solver *prev,*next;

	CPtrTurnType reorderTurns(CPtrTurnType pt,CPtrTurnType gr,const int cgr);

	int solve(int alpha,int beta,PtrTurnType gr,const int cgr,const bool allowFromSetOnly);

	//solve last layer
	int solveLast(int alpha,int beta,CPtrTurnType gr,const int cgr,const bool allowFromSetOnly);

	//heuristic algorithm	can be crush of larmouth
	int solveHeuristic(int alpha,int beta,PtrTurnType gr,const int cgr,const bool allowFromSetOnly);

	void setIn(CPtrTurnType gr,const int cgr);

#ifdef USE_HASH
	HashItem hashItem;
	void fillHash(int estimate,char flag);
	int checkHash(CPtrTurnType gr,const int cgr,int beta,bool allowFromSetOnly,bool heuristic);
#endif

public:
#ifdef COUNT_NODES
	static unsigned nodes;
#endif

#ifdef USE_HASH
	static HashItem*hashTable;
#ifdef COUNT_HASH_NODES
	static unsigned hashCounter;
#endif
#endif

	/*
	count subgroups for turns[0..ct-1] with response[0..ct-1] and last turn t[ct]
	out  - out string can be NULL
	outR - array of possible responses 	

	t[]  - array of turns
	r[]  - array of responces
	ct   - turns counter
	par  - output type (0..2) 2- to store tree
	NOTE if par=2:outR[BC(1,POSITIONS)]=true means found turn BC(POSITIONS,0)
	return outR[]=true if item from set
	*/
	static bool subgroups(char*out,bool*outR,const int* t,const int* r, int ct,int par);

#ifndef _CONSOLE
	inline static CWnd* getOut(int n=0){
		return sOut[n];
	}
#endif

	/*
	alpha - lower estimate
	beta  - upper estimate
	depthCount - //=7 count 7th,=6 count 6th...
	t[] - array of turns
	r[] - array of responces
	ct  - count of turns = number of turns

	mde - max depth for exact count (if (depth>mde && not_last_layer) use crush algorithm otherwise use exact algorithm)
	      if mde=3 than for turns 4,5 use crush algorithm, for last layer always use exact (if depthCount=7)
	
	allowFromSetOnly - allows number olny from recent numbers
	*/
	static int estimate(const int alpha,int beta,const int depthCount,const int* t,const int* r,const int ct,
		const int mde,const bool allowFromSetOnly);

	static int getBest();

	static void init();
	static void deInit();

};

#endif
