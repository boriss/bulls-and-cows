// bcw.cpp : Defines the class behaviors for the application.
//
/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

#include "stdafx.h"
#include "bcw.h"
#include "bcwDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CBcwApp

BEGIN_MESSAGE_MAP(CBcwApp, CWinApp)
	//{{AFX_MSG_MAP(CBcwApp)
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBcwApp construction

CBcwApp::CBcwApp(){
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBcwApp object

CBcwApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CBcwApp initialization
BOOL CBcwApp::InitInstance(){

	CBcwDlg dlg;

	m_pMainWnd = &dlg;
	dlg.DoModal();

	return FALSE;
}
