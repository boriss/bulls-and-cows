/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
created 7jun2014
*****************************************************************************************************************/

#ifndef _HASH_H_
#define _HASH_H_

const unsigned HASH_BITS=19;//19 for executor 20 for bcw
const unsigned HASH_SIZE=1<<HASH_BITS;//2^HASH_BITS items only power of two is allowed

const unsigned char HASH_EXACT=1;
const unsigned char HASH_BETA=0;

const int HASH_CODE_SIZE=MAXN/32;// 5040/32=157 5040=157*32+16, 1296/32=40 1296=32*40+16

class HashItem{
public:

	inline HashItem(){
		setEmpty();
	}

	//code of item
	unsigned code[HASH_CODE_SIZE];
	union{
		unsigned additionalCode;
		struct{
			unsigned scode:16;
			unsigned mde:3;//algorithm type
			unsigned allowFromSetOnly:1;
			unsigned heuristic:1;
		};
	};

	union{
		unsigned __int64 data;

		struct{
			//additional parameters
			unsigned estimate:15;
			unsigned flag:1;
			unsigned depth:3;
			unsigned turn:13;
		};
	};

	bool operator==(const HashItem& item)const;
	void operator=(const HashItem& item);
	void set(const char a[],const int _mde,bool _allowFromSetOnly,bool _heuristic);
	void set(int _estimate,int _depth,int _turn,char _flag);
	unsigned hash()const;

	inline void setEmpty(){mde=0;}
	inline bool isEmpty()const{return mde==0;}
	inline bool isNotEmpty()const{return mde!=0;}
};

#endif//#ifndef _HASH_H_