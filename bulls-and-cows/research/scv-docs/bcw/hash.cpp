/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
created 7jun2014
*****************************************************************************************************************/

#include "StdAfx.h"
#include "hash.h"

bool HashItem::operator==(const HashItem& item)const{
	if(additionalCode!=item.additionalCode){
		return false;
	}

	unsigned const* p=code;
	unsigned const* p1=item.code;
	for(int i=0;i<HASH_CODE_SIZE;i++,p++,p1++){
		if(*p!=*p1){
			return false;
		}
	}

	return true;
}

void HashItem::operator=(const HashItem& item){
	unsigned* p=code;
	unsigned const* p1=item.code;
	for(int i=0;i<HASH_CODE_SIZE;i++,p++,p1++){
		*p=*p1;
	}

	additionalCode=item.additionalCode;
	data=item.data;
}


void HashItem::set(const char a[],const int _mde,bool _allowFromSetOnly,bool _heuristic){
	unsigned* p=code;
	const char*pa=a;
	int i,j;
	for(i=0;i<HASH_CODE_SIZE;i++,p++){
		*p=0;
		for(j=0;j<32;j++,pa++){
			if(*pa){
				(*p)^=1<<j;
			}
		}
	}
	additionalCode=scode=0;
	for(j=0;j<16;j++,pa++){
		if(*pa){
			scode^=1<<j;
		}
	}
	mde=_mde;
	allowFromSetOnly=_allowFromSetOnly;
	heuristic=_heuristic;

}

void HashItem::set(int _estimate,int _depth,int _turn,char _flag){
	estimate=_estimate;
	depth=_depth;
	turn=_turn;
	flag= _flag;
}

unsigned HashItem::hash()const{
	unsigned c = additionalCode^(additionalCode>>HASH_BITS);
	unsigned const* p=code;
	for(int i=0;i<HASH_CODE_SIZE;i++,p++){
		c^=*p^(*p>>HASH_BITS); //16%fill for mm
	}
	return c&(HASH_SIZE-1);
}

