/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#ifndef _TREE_NODE_
#define _TREE_NODE_

const int MAX_TREE_NODES=MAXN*2;

//this class using to create tree of best turns
class TreeNode{

public:
	TreeNode(){
	}
	TreeNode::TreeNode(int _minTurns,int _estimate,int _turn,int _layer, int _id, int _response,int _parentId);

	//unique number of item
	int id;

	//pointer of children for every response; =0 indicates impossible response
	TreeNode* child[MBC];

	/*
	if nth=5 then all numbers could be solved for 5 turns, 
	and estimate is count of numbers which can be solved using five turns
	*/
	int nth;
	int estimate;

	//best turn; turn=0 means that it's last turn
	int turn;

	//number of layer 0 - for root, 1- for it's children ...
	int layer;

	//from which response this child appear, using only for building tree
	int response;

	//pointer to parent
	TreeNode* parent;

	//
	bool hasChildren()const;

	void set(int _nth,int _estimate,int _turn,int _layer, int _id, int _response, 
		TreeNode* _parent,TreeNode** _child);

	//void load(FILE*f);
	void store(FILE*f);

	//const char* uses for console application
	static void storeHelperString(FILE*f,int nItems, const char* algorithmName);

	bool needStore()const;
};

#endif