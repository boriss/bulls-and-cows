// bcwDlg.cpp : implementation file
//

/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

/*
============================ VERSION_HISTORY ===================================
VERSION 3.0 - nov2013+may,june2014
	site{
		update executor on sources, add batch files at least
		statistics description turns % from set
		menu on html page new set of algorithms for tree browsing
		[n] - tree node description n- amount of recent numbers
		ref -slovesnov.users.SF.net/
	}
	article{
		���� ������� ��� ������� ������ ����� �� ����������� ��������� � ��������� ������� 0123 0.1+t 0.2 ���� ���������, ��� ��� ��� ������ ����� ��������� �����, � ���� 4
		equality splits about two time faster [mastermind 40 sec->28sec] [bullscows was 3h:14m ->1h:40m]
	}

	minimize avg mastermind{
	game length=6
		0012 BC11+0304 BC11+0145 estimate=184 (if max game length=5)
		0012 BC11+0304 BC11+2044 estimate=183 (if max game length=6)
	}


	reorder stored turns (1-3 slowdown)
	add percentage of best turns from set (javascript)
	add mastermind solution (6 digits,4 positions,digits can repeated)
	add alpha,beta params for solveLast and cutoffs
	add cutoffs for prelastLayer
	add move ordering (always check turns from recent numbers first) [bug fixed for code from ~28mar2013]
	remove turns1245_00,SOLVE_0123_01_1245_00
	removed creation of html tree (now in javascript)
	javascript tree optimization removed "!"-separator

VERSION 2.0 - ??nov2011
	common{
		exclude number "s" from S_3(s) set
		add SOLVE_0123_01_1245_00 macro to fast solve 0123 0.1+1245 0.0 (checked for crush35,crush45,crush5) 
			This case is solved by bcw program
		add #define _USE_MEMORY_FOR_RESPONSE macro If defined then program allocates memory with precounted response table, 
			otherwise not use memory, needed when uses a lot of threads of executor, program became slower (in two times)
		solve with exact algorithm sequence 0123 0.1+1245 0.0, have 1 7th (using bcw program)
		implement minimizing average game length algorithm 
		implement larmouth heiristic algorithm
	}
	bcService{
		set Idle priority for all processes, now processes do not hamper other processes
		set 4 processes instead of 3 by default, because I've four processors
	}
	bcw{
		fixed bug with counting left time
	}
	executor{
		fixed bug. When setup 3st three numbers with responses. Executor started from beginnig after restart, now start from stored immediate point.
		fixed bug with mutex, now prevent from multirunning from service as well
		now works for unix (executor uses 1 file for each process)
		add taker jobs from file jobs.txt for unix
	}
	new project shutdown{
		automaticall turn off computer when electricity supply meter goes on day tariff which is more expensive than night one.
		Uses for long calculation of executor.
	}

VERSION 1.0 - 22aug2011

*****************************************************************************************************************/

#include "stdafx.h"
#include "bcw.h"
#include "bcwDlg.h"
#include "numbers_group.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "tree_string.h"
#include "solver.h"
#include "number.h"
#include "transposition.h"
#include "turns.h"
#include "treeNode.h"

CBcwDlg* dlg=0;

/////////////////////////////////////////////////////////////////////////////
// CBcwDlg dialog

CBcwDlg::CBcwDlg(CWnd* pParent /*=NULL*/): CDialog(CBcwDlg::IDD, pParent){
	//{{AFX_DATA_INIT(CBcwDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	canClose=false;
}

void CBcwDlg::DoDataExchange(CDataExchange* pDX){
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CBcwDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CBcwDlg, CDialog)
	//{{AFX_MSG_MAP(CBcwDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_STORE, OnButtonStore)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CBcwDlg message handlers

UINT thread(LPVOID){
	CString s;
	clock_t start=clock();


	//using init&deinit in one function allow avoid memory leaks(recognized by vc++)
	Number::init();
	Solver::init();
	Transposition::init();

#ifdef _BULLS_COWS_

	//5-avg5|crush5o
	//dlg->storeTree(5,false);

	//dlg->storeTurns();

	dlg->createJavaScriptStringTreeAll();
	//dlg->createJavaScriptStringTree("treeAvg5.txt",true);
	//dlg->createJavaScriptStringTree("treeCrush5o.txt",true);

	//dlg->solveSubgroupsResponse(false);
	//dlg->solveSubgroups(false);

	//dlg->createStringForCrush();

	//dlg->countNumbers();

	//dlg->showSubgroups();

	//dlg->countNumbers();
	//dlg->solveAll();

	//dlg->test();

#else //_BULLS_COWS_

	dlg->createJavaScriptStringTreeAll();
	//storeTree(4,false) - avg(max 5 turns) | exact for crush
	//storeTree(5,false) - avg(max 6 turns) avg only
	//dlg->storeTree(5,false);

	//dlg->createJavaScriptStringTree("treeAvgMastermind5.txt",true);
	//dlg->createJavaScriptStringTree("treeAvgMastermind.txt",true);
	//dlg->createJavaScriptStringTree("treeCrushMastermind.txt",true);

	//dlg->storeTurns();
	//dlg->solveSubgroupsResponse(false);
	//dlg->solveSubgroups(false);
	//dlg->showSubgroups();
	//dlg->test();

#endif

	Number::deInit();
	Transposition::deInit();
	Solver::deInit();

	int dt=int((clock()-start)/(double)CLOCKS_PER_SEC);
	s.Format("END duration %02d:%02d:%02d",dt/3600,(dt/60)%60,dt%60);

	dlg->SetWindowText(s);

	dlg->canClose=true;
	return 0;
}

void CBcwDlg::solveSubgroups(const bool allowFromSetOnly){
	CString s,s1,s2,s3;
	int i,j,e,dt;

#ifdef _BULLS_COWS_
	const int MDE_START=2;
	int t[]={  123,1045};
	const int st=sizeof(t)/sizeof(t[0]);
	int r[st]={ BC01 };
	const int count=7;//NOTE #ifdef _MINIMIZE_AVERAGE_ count=7 ALWAYS!
	const int MAX_TURN=6;//MAX_TURN=6 - is the exact algorithm
#else
	//*
	int t[]={  11};//123 12 11 1
	const int MDE_START=2;
	const int MAX_TURN=4;
	const int count=5;
	const int st=sizeof(t)/sizeof(t[0]);
	int r[st]={ BC00};
	//*/

	/*search avg with depth=6
	int t[]={  123};//123 12 11 1
	const int MDE_START=2;
	const int MAX_TURN=4;
	const int count=6;
	//*/

#endif

	if(allowFromSetOnly){
		s1=" fromSetOnly";
	}
	s="solveSubgroups"+s1+" ";

//do cycle for turn[1]
//#define CYCLE_TURN

	int bestT[MBC],bestE[MBC];//store bestTrun&bestEstimate
	for(i=0;i<MBC;i++){
		bestT[i]=0;
		bestE[i]=0;
	}

	#if defined(_BULLS_COWS_) && defined(_MINIMIZE_AVERAGE_)
	if(count!=7){
		MESSAGE("ERROR 247");
		return;
	}
	#endif

	const int startOutput=1;

//#define CYCLE_TURN

#ifdef CYCLE_TURN
int _cnt=0;
#ifdef _BULLS_COWS_
//cycle by 2ndturn
//for(int it=0;it<TURN2C;it++){t[1]=Array[TURN2[it]].Int();

//BEGIN cycle by 3rd turn
//1. find t[1] index
for(j=0;j<TURN2C;j++){
	if(t[1]==Array[TURN2[j]].Int()){
		break;
	}
}
if(j==TURN2C){
	MESSAGE("ERROR 190");
	return;
}

const int *pi;
const int ALL=TURN3C[j];
const int TH=3;
const int SZ=ALL/TH;

for( _cnt=0,pi=TURN3[j] ; *pi!=-1 ; pi++,_cnt++ ){
	t[2]=Array[*pi].Int();

//END cycle by 3rd turn
#else//_BULLS_COWS_
	for(_cnt=0;_cnt<TURN1C;_cnt++){
		t[0]=Array[TURN1[_cnt]].Int();
#endif//_BULLS_COWS_
#endif

	char out[4096];
	bool outR[MBC];
	bool bc40=Solver::subgroups(out,outR, t,r,st-1,1 );
	s1=out;
	s1=s1.SpanExcluding("\r\n");
	s+=s1;
	#ifdef _MINIMIZE_AVERAGE_
	s1=" minimize average";
	#else
	s1.Format(" count %dth",count);
	#endif 
	s+=s1+"\r\n";

	int est[MBC];
	for(i=0;i<MBC;i++){
		est[i]=outR[i]?MAXE:0;
	}

	clock_t tm,ts;

	for(j=MDE_START;j<MAX_TURN;j++){
		
		ts=clock();
		if(j>startOutput){
			s1="";
			appendCharsToLength(s1,56,'-');
			s+=s1+"\r\n";
			Solver::getOut()->SetWindowText( s );
		}
		for(i=0;i<MBC;i++){
			if(est[i]==0){
				continue;
			}
			r[st-1]=i;

			if(j>startOutput){
				s3=getAlgorithmString(j,count,7);
				s1.Format("%s (%d.%d) beta=%5d ",s3,BULL(i),COW(i),est[i]);

				s+=s1;
				Solver::getOut()->SetWindowText( s );
			}

			tm=clock();

			int bt=-1;

			e=Solver::estimate(0, est[i], count, t, r, st,j,allowFromSetOnly );

			bt=Solver::getBest()==-1 ?-1 : Number::array[Solver::getBest()].Int();

			if(j>startOutput){

				dt=int((clock()-tm)/(double)CLOCKS_PER_SEC);
				s1.Format("e=%5d time %02d:%02d:%02d best=%04d\r\n",e>9999?9999:e,dt/3600,(dt/60)%60,dt%60,
					bt/*Solver::getBest()==-1 ?-1 : Array[Solver::getBest()].Int() */
					);
				s+=s1;
				bestT[i]=bt;//Solver::getBest()==-1 ?-1 : Array[Solver::getBest()].Int();
				bestE[i]=e;
			}
			Solver::getOut()->SetWindowText( s );
			//after output
			if(e<est[i]){
				est[i]=e;
			}
				
		}//for(i=0;i<MBC

		//show response 4.0
		#ifdef  _MINIMIZE_AVERAGE_
		s1.Format("%s (4.0) beta=%5d e=%5d time 00:00:00 best=0123\r\n",s3,st,st);
		s+=s1;
		Solver::getOut()->SetWindowText( s );
		#endif


		if(j>startOutput){
			for(e=i=0;i<MBC;i++){
				e+=est[i];
			}

			//add one if minimize avg, because of response(4.0)
			#ifdef  _MINIMIZE_AVERAGE_
			e+=st;
			#endif


			dt=int((clock()-ts)/(double)CLOCKS_PER_SEC);
			s1.Format("total %s",formatNumber(e));
			#ifdef  _MINIMIZE_AVERAGE_
			s2.Format(" average=%.4lf",double(e)/MAXN);
			s1+=s2;
			#endif

			appendCharsToLength(s1,33);
			s2.Format("time %02d:%02d:%02d\r\n",dt/3600,(dt/60)%60,dt%60);
			s1+=s2;

			s+=s1;
		}

		Solver::getOut()->SetWindowText( s );

	}
#ifdef CYCLE_TURN
}
#endif

#ifdef _BULLS_COWS_
//BEGIN STORE FOR EXACT
for(i=0;i<MBC;i++){
	if(bestE[i]==0){
		continue;
	}

	s1.Format("solve 0123(%d,%d)+%04d(%d,%d) %d %d %d #\r\n",BULL(r[0]),COW(r[0]),t[1],BULL(i),COW(i),
		count,count,bestE[i]);
	s+=s1;
}
//END STORE FOR EXACT
#endif

	s+="the end\r\n";
	Solver::getOut()->SetWindowText( s );
#undef CYCLE_TURN	
}

void CBcwDlg::solveSubgroupsResponse(const bool allowFromSetOnly){
	CString s,s1,s2,s3;
	int i,j,e,dt;

	int est=MAXE;

//do cycle turns
//#define CYCLE_TURN

#ifdef _BULLS_COWS_
	const int MDE_START=2;
	int t[]={  123,1045 };
	int r[]={ BC01,BC00 };
	int count=7;//7 count 7th
	const int st=sizeof(t)/sizeof(t[0]);
	const int MAX_TURN=6;//MAX_TURN=6 - is the exact algorithm

	//params (#ifdef CYCLE_TURN then fill both t&r, or set size manually int t[2],int r[2])
	//int t[]={  123,1245,5634};
	//int r[]={ BC01,BC01,BC01 };

#else
//max 5 turns
	const int MDE_START=2;
	int t[2]={12,3344};
	int r[2]={0,10};
	const int st=2;
	int count=5;
	const int MAX_TURN=4;
//*/

/*max 6 turns
	const int MDE_START=2;
	int t[]={  12};
	int r[]={BC02};
	const int st=sizeof(t)/sizeof(t[0]);
	int count=6;
	const int MAX_TURN=5;
//*/

#endif


#ifdef CYCLE_TURN
	int sc,it;
	#ifdef _BULLS_COWS_
	for(sc=it=0;it<TURN2C;it++){
		if(Array[TURN2[it]].onlyDigits0123()){//skip 0123
			continue;
		}
		t[1]=Array[TURN2[it]].Int();
		sc++;
	#else//_BULLS_COWS_
		st=1;
	for(sc=it=0;it<TURN1C;it++){
		t[0]=Array[TURN1[it]].Int();
		sc++;
	#endif
#endif

	if(allowFromSetOnly){
		s1=" fromSetOnly";
	}
	s3.Format("solveSubgroupsR%s solving ",s1);
	s+=s3;
	for(i=0;i<st;i++){
		s1.Format( "%04d(%d.%d)%s",t[i],BULL(r[i]),COW(r[i]),i+1==st?"":"+");
		s+=s1;
	}

	#ifndef _MINIMIZE_AVERAGE_
	s1.Format(" count %dth",count);
	s+=s1;
	#else
	s+=" minimize average";
	#endif

	#ifdef CYCLE_TURN
	s1.Format(" (%d)",sc);
	s+=s1;
	#endif

	s+="\r\n";

	clock_t tm;

	for(j=MDE_START;j<MAX_TURN;j++){

		s3=getAlgorithmString(j,count,7);
		s1.Format("%s beta=%5d ",s3,est);
		s+=s1;

		Solver::getOut()->SetWindowText( s );

		tm=clock();

		e=Solver::estimate(0, est, count, t, r, st,j,allowFromSetOnly );

		dt=int((clock()-tm)/(double)CLOCKS_PER_SEC);
		s1.Format("e=%5d best=%04d time %02d:%02d:%02d mde=%d\r\n",e,
			Solver::getBest()==-1 ?-1 : Number::array[Solver::getBest()].Int(),
			dt/3600,(dt/60)%60,dt%60
			,j);
		s+=s1;
		Solver::getOut()->SetWindowText( s );

		//after output
		if(e<est){
			est=e;
		}
			
		Solver::getOut()->SetWindowText( s );

		if(est==0){
			break;
		}

	}
#ifdef CYCLE_TURN
}
#endif

	#ifdef COUNT_NODES
	s+="nodes="+formatNumber(Solver::nodes)+"\r\n";
	#endif

	Solver::getOut()->SetWindowText( s+"the end\r\n" );

#undef CYCLE_TURN		
}

void CBcwDlg::countNumbers(){
	CString s;
	int i,j,c;

	int t[]={  123,1245,5634,9840,7592 };
	int r[]={ 1,1,1,6,10 };
	const int st=sizeof(t)/sizeof(t[0]);

	int ti[8];
	for(i=0;i<st;i++){
		j=Number::findIndex(t[i]);
		if(j==-1){
			MESSAGE("couldnt find index");
			return;
		}
		ti[i]=j;
	}

	for(c=i=0;i<MAXN;i++){
		for(j=0;j<st;j++){
			if(RESPONSE(ti[j],i)!=r[j]){
				break;
			}
		}
		if(j==st){
			c++;
		}
	}
	s.Format("countNumbers\r\ncnt=%d",c);
	Solver::getOut()->SetWindowText( s );
}

//count subgroups
void CBcwDlg::showSubgroups(){
	CString s;

//items=1 0123(0,3)+4567(0,1)+1274(0,1)+5302(0,3)+3051(0,2)+0000(*,*)
//	items=4 0123(0,3)+4567(0,1)+1274(0,1)+5302(0,3)+3051(*,*) - parent

#ifdef _BULLS_COWS_
	int t[]={  123,1435,4362,4019};
	int r[]={ BC02,BC02,BC10,BC02 };
#else

	int t[]={ 12, 304,  145,135};
	int r[]={6,6,15};

#endif


	const int st=sizeof(t)/sizeof(t[0]);

	char out[4096];
	bool outR[MBC];
	Solver::subgroups(out,outR, t,r,st-1,0 );
	s=out;
	s.Replace("\n","\r\n");

	Solver::getOut()->SetWindowText( s );
	
}

//cycle for count first solve 7th, next 6th ....
void CBcwDlg::solveAll(){
  CString s,s1;
	clock_t tm=clock();

	int startCount=7;//count 7=7th
	int startEstimate=  MAXN;
	int mde=2;//from mde+1 use crush algorithm

	int i,j;


	int t[]={   123, 1245  };
	int r[]={ BC01,BC01  };
	const int st=sizeof(t)/sizeof(t[0]);

	s1.Format("solving(%dth) ",startCount);
	s+=s1;
	for(i=0;i<st;i++){
		s1.Format("%04d(%d.%d)%s", t[i],BULL(r[i]),COW(r[i]),i+1==st ?"\r\n":"+" );
		s+=s1;
	}
	Solver::getOut()->SetWindowText(s);


	for(i=startCount;i>1;i--){

		int e=Solver::estimate( 0,startEstimate, i, t, r, st,2,false );
		
		s1.Format("%dth e=%d\r\n",i,e);
		s+=s1;
		Solver::getOut()->SetWindowText(s);

		if(e>0){
			j=int((clock()-tm)/(double)CLOCKS_PER_SEC);
			s1.Format("the end proceed %02d:%02d:%02d\r\n", j/3600,(j/60)%60,j%60 );
			s+=s1;

			for(j=0;j<7-i;j++){
				s1.Format(" --");
				s+=s1;
			}
			s1.Format(" %2d",e);
			s+=s1;			
			Solver::getOut()->SetWindowText(s);
			break;
		}
	}

}

BOOL CBcwDlg::OnInitDialog(){//###
	CString s,q;

	CDialog::OnInitDialog();
  dlg=this;
	setTimeString(true);
	SetWindowText("bucowindows "+startTimeS);

	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

#ifdef _BULLS_COWS_
	s="bulls cows";
#else
	s="mastermind";
#endif

	s+=" ";

#ifdef _MINIMIZE_AVERAGE_
	s+="avg";
#else
	s+="Nth";
#endif

	s+=" ";
/*
#ifdef USE_HASH
	s+="hashOn";
#else
	s+="hashOff";
#endif
*/
	s+=" ";

#ifdef _DEBUG
	s+="debug";
#else
	s+="release";
#endif

	GetDlgItem(IDC_STATUS)->SetWindowText(s);

	AfxBeginThread( thread , 0 , THREAD_PRIORITY_NORMAL );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CBcwDlg::OnPaint() {
	if (IsIconic()){
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else{
		CDialog::OnPaint();
	}
}

HCURSOR CBcwDlg::OnQueryDragIcon(){
	return (HCURSOR) m_hIcon;
}

void CBcwDlg::OnClose(){
	if(mbCheck()){
		CDialog::OnClose();
	}
}

void CBcwDlg::OnCancel(){
	if(mbCheck()){
		CDialog::OnCancel();
	}
}

void CBcwDlg::OnOK(){
	if(mbCheck()){
		CDialog::OnOK();
	}
}

bool CBcwDlg::mbCheck(){
	if(canClose){
		return 1;
	}

	if(MessageBox("Do you really want to exit?","question",MB_YESNO)==IDNO ){
		return 0;
	}

	//when we try to close both fuctions OnCancel&OnClose calling
	canClose=true;

	return 1;
}


void CBcwDlg::storeTurns(){
	int i,j,t2c;
	CString s,s1;
	int eq[MAXN],t2[MAXN];
	int eqc=0;
	int t3[MAXN];
	int ct3=0;

	const int TC=ALLOW_SAME_DIGITS?1:2;


	Transposition::init();

	s1.Format("turns.txt");
	FILE*f=fopen(s1,"w+");
	if(!f){
		MESSAGE("1003cannt open file to write");
		return;
	}
	fprintf(f,"//this is automatically generated file see CBcwDlg::storeTurns()\n");
	fprintf(f,"//this generates turns%d-%d\n",TC,TC+1);

#ifdef _ALLOW_SAME_DIGITS_
	eq[0]=0;
#endif
	eqc=ALLOW_SAME_DIGITS;

	int N[]={0};

	for(i=1;i<MAXN;i++){
		for(j=0;j<eqc;j++){

			if(Transposition::isEquivalent(N,TC-1,i,eq[j]) ){
				break;
			}
		}//for(j)
		if(j==eqc){
			eq[ eqc++ ]=i;
		}
		
	}

	//store in invert order
	fprintf(f,"const int TURN%dC=%d;\n",TC,eqc);
	fprintf(f,"CTurnType TURN%d[%d]={\n",TC,eqc+1);
	for(i=0;i<eqc;i++){
		t2[i]=eq[eqc-1-i];
		fprintf(f,"%4d,//%04d\n",t2[i],Number::array[t2[i]].Int() );
	}

	fprintf(f,"  -1\n};\n");

	const t2s=eqc;
	int totalItems=0,maxItems=0;

	//0123, TURN2, from set, Array[j]
	for(t2c=0;t2c<t2s;t2c++){

		s1.Format("t2c=%d/%d",t2c,t2s );
		Solver::getOut()->SetWindowText(s1);

		for(eqc=0,i=1;i<MAXN;i++){
			for(j=0;j<eqc;j++){
				int N[]={
#ifndef _ALLOW_SAME_DIGITS_
				0,
#endif
				t2[t2c]
				};
				if(Transposition::isEquivalent(N,sizeof(N)/sizeof(N[0]),i,eq[j])){
					break;
				}
			}
			if(j==eqc && i!=t2[t2c]){//added i!=t2[t2c] exclude number "s"
				eq[ eqc++ ]=i;
			}

		}

		//store in invert order
		t3[ct3++]=eqc;
		s1.Format("{//%04d %d item(s)\n",Number::array[t2[t2c]].Int(),eqc);
		s+=s1;
		for(i=0;i<eqc;i++){
			s1.Format("%4d,\n",eq[eqc-1-i] );
			s+=s1;
		}
		s+="-1\n},\n";

		totalItems+=eqc;
		if(eqc>maxItems){
			maxItems=eqc;
		}

	}//t2

	//store counters of t3
	fprintf(f,"const int TURN%dC[]={\n",TC+1);
	for(i=0;i<ct3;i++){
		fprintf(f,"%4d,\n",t3[i]);
	}
	fprintf(f,"};\n");

	//store turn3
	fprintf(f,"CTurnType TURN%d[%d][%d]={\n",TC+1,t2s,maxItems+1 );
	
	fprintf(f,"%s};//totalItems=%d maxItems=%d\n",s,totalItems,maxItems+1);

	Solver::getOut()->SetWindowText("end");
	dlg->SetWindowText("END");

	fclose(f);

}

void CBcwDlg::OnButtonStore(){
	CString s;
	Solver::getOut()->GetWindowText(s);
	FILE*f=fopen("out.txt","wb+");
	if(f){
		fputs(s,f);
		fclose(f);
		MESSAGE("store ok");
	}
	else{
		MESSAGE("error 504");	
	}
}

void CBcwDlg::setTimeString(bool p){
	time_t     now;
  struct tm  *ts;
  now = time(0);
  ts = localtime(&now);
	char s[256];
  strftime(s, 256, "%d-%b-%Y %H:%M:%S", ts);

	CString as;
	if(p){
		as="started at ";
	}
	else{
		as="finished at ";	
	}
	as+=s;

	if(p){
		startTimeS=as;
	}
	else{
		endTimeS=as;
	}
}


bool CBcwDlg::checkFileExists(const char* fName){
	FILE*f=fopen(fName,"rb");
	if(!f){
		return false;
	}
	fclose(f);
	return true;
}

void CBcwDlg::proceedFile(const char* fName,int& e,int& t,int& nth,bool thirdTurn/*=true*/){
	int i;
	char b[128],*p;
	const char COUNT[]="count ";
	const char MAX[]="max=";
	FILE*f=fopen(fName,"rb");
	if(!f){
		MESSAGE("cannt open file1036["+CString(fName)+"]");
		return;
	}

	for(i=0;fgets(b,128,f);i++){
		if(i==1){
			p=strstr(b,COUNT);
			if(p==0){
				MESSAGE("error p==0 ["+CString(b)+"]"+CString(fName));
				return;
			}
			p+=strlen(COUNT);
			nth=atoi(p);

		}
	}
	if(b[0]=='/'){//there is no best turn for such estimate
		t=-1;
		p=strstr(b,MAX);
		if(p==0){
			MESSAGE("error p==0 ["+CString(b)+"]"+CString(fName));
			return;
		}
		//e=startEstimate
		e=atoi(p+strlen(MAX));
	}
	else{
		e=atoi(b+41);
		if(thirdTurn){
			t=atoi(b+26);
		}
		else{
			t=atoi(b+16);
		}
	}

	fclose(f);
}

void CBcwDlg::storeTree(const int _alg,const bool allowFromSetOnly){
	int alg=_alg;

#ifdef _BULLS_COWS_
	if(allowFromSetOnly){
		alg=6;//since exactAlg give same result with larmouth5 set MDE=5;
	}
	else{
		if( alg!=5 ){
			MESSAGE("error 1281");
			return;
		}
	}

	const int NTH=7;
#else
	if(alg!=4
	#ifdef _MINIMIZE_AVERAGE_
		&& alg!=5
	#endif
	){
		MESSAGE("storeTree error 1502 invalid algorithm type");
		return;
	}
	const int NTH=alg==5?6:5;
#endif

	const int MDE = alg-1;

	TreeNode tree[MAX_TREE_NODES];
	TreeNode *it,*pr;

	int t[8]={
#ifdef _BULLS_COWS_
		123
#else
		12
#endif
	};

	int r[8];
	int id=0;
	int i,e,turn,layer=0;
	FILE*f;
	CString s,s1;
	bool outR[MBC];
	int nth,est;
	int sc=0,ec=0;
	CString algS;
	char b[128];
	char*pc;

	const CString ALGNAME=getShortAlgorithmName(true)+(allowFromSetOnly?"FromSetOnly":"");
#ifdef _BULLS_COWS_

	if(allowFromSetOnly){
		algS=ALGNAME;
	}
	else{
		algS.Format("%s%d%s%s",ALGNAME,alg,alg==5?"":"5",
		#ifdef _LARMOUTH_
		""
		#else
		"o"
		#endif
		);
	}
#else
	algS=ALGNAME+"Mastermind";//always exact & optimized so skip "Exact" & "o" in name
	#ifdef _MINIMIZE_AVERAGE_
	if(alg==4){
		algS+='5';
	}
	#endif
#endif
	s.Format("store tree alg=%s\r\n",algS);
	Solver::getOut()->SetWindowText( s );
	clock_t tm=clock();

	//layer0 we'll set estimate below now use MAXE
	tree[id].set(NTH,MAXE,t[0],0,id,-1,NULL,NULL);
	id++;

	//layer 1 means 2nd turn
	for(layer=1;layer<NTH;layer++){
		s1.Format("proceed layer%d [%d %d]\r\n",layer,ec,id-1);
		s+=s1;
		Solver::getOut()->SetWindowText( s );

		for(sc=ec,ec=id,it=tree+sc;it<tree+ec;it++){

			if(it->turn==-1){
				continue;
			}

			s1.Format("%d/%d",int(it-tree)-sc,ec-sc);//we use several answers!,so counter can go downstairs
			Solver::getOut(1)->SetWindowText( s1 );

			//setup t,r except last response [0..layer-1]
			for(pr=it,i=0;i<layer;i++,pr=pr->parent){
				t[layer-1-i]=pr->turn;

				if(i+1!=layer){
					r[layer-2-i]=pr->response;
				}
			}

			//find all possible turns
			Solver::subgroups(NULL,outR, t,r,layer-1,2 );

			//NOTE outR[BC(1,POSITIONS)]=true means found turn BC(POSITIONS,0) see subgroups function
			for(est=it->estimate,i=0;i<MBC;i++){

				if(outR[i]==false){
					continue;
				}

				if(i==BC(1,POSITIONS)){//Solver::subgroup move BC(POSITIONS,0)->BC(1,POSITIONS)
					#ifdef _LARMOUTH_
					est-=layer+1;
					#endif
					continue;
				}

				//setup last response before 6th
				r[layer-1]=i;

#ifdef _MINIMIZE_AVERAGE_
				nth=NTH;
				if(layer==1 
	#ifdef _BULLS_COWS_
					|| layer==2
	#endif
					){
					if(layer==1){
						sprintf(b,"$(%d.%d)",BULL(r[0]),COW(r[0]));
					}
					else{
						sprintf(b,"$(%d.%d)%04d(%d.%d)",
							BULL(r[0]),COW(r[0]),
							t[1],BULL(r[1]),COW(r[1])
							);
					}
					
	#ifdef _BULLS_COWS_
					pc=strstr(allowFromSetOnly?FROM_SET_ONLY_STR:ALG_FIRST_TURNS_STRING,b);
	#else
					pc=strstr(ALG_FIRST_TURNS_STRING,b);
	#endif
					if(pc==NULL){
						MESSAGE("error1527["+CString(b));
						return;					
					}
					pc=strstr(pc,"e=");
					if(pc==NULL){
						MESSAGE("error1532");
						return;					
					}
					e=atoi(pc+2);
	#ifndef _BULLS_COWS_
					if(alg==5 && layer==1 && r[0]==BC11  ){//avg6
						e--;
					}
	#endif

					pc=strstr(pc,"best=");
					if(pc==NULL){
						MESSAGE("error1539");
						return;					
					}
					turn=atoi(pc+5);

				}
				else{
		
					int _mde=MDE;
					nth=NTH;
					bool _allowFromSetOnly=allowFromSetOnly;
		#ifdef _BULLS_COWS_
					if(alg==5 && allowFromSetOnly==false){
						if( (r[0]==BC01 || r[0]==BC11) &&(r[1]==BC01||r[1]==BC02||r[1]==BC11) ||
							r[0]==BC02 && (r[1]==BC01 || r[1]==BC02 || r[1]==BC10 || r[1]==BC11)
							){//use avg5

						}
						else if(r[0]==BC03 && r[1]==BC11 || r[0]==BC01 || r[0]==BC11 || r[0]==BC02 ){//use avg45	
							_mde--;
						}
						else{//use fso5
							_allowFromSetOnly=true;
						}
					}
		#else
					//24may2014
					if(alg==5){//avg6
						if(layer>1 && r[0]==BC11 && r[1]==BC11){
						}
						else{
							nth--;
						}
					}

		#endif
					e=Solver::estimate(0, est+1  , nth, t, r, layer, _mde,_allowFromSetOnly);//HAVE TO USE est+1!!
					if(Solver::getBest()==-1){
						MESSAGE("error1515");
						return;
					}
					turn=Number::array[Solver::getBest()].Int();
				}

#else//_MINIMIZE_AVERAGE_
				if(layer==1 
	#ifdef _BULLS_COWS_
					|| layer==2
	#endif
					){
					if(layer==1){
						sprintf(b,"$(%d.%d)",BULL(r[0]),COW(r[0]));
					}
					else{
						sprintf(b,"$(%d.%d)%04d(%d.%d)",
							BULL(r[0]),COW(r[0]),
							t[1],BULL(r[1]),COW(r[1])
							);
					}
					
					pc=strstr(ALG_FIRST_TURNS_STRING,b);
					if(pc==NULL){
						MESSAGE("error1527["+CString(b));
						return;					
					}

					pc=strstr(pc,"nth=");
					if(pc==NULL){
						MESSAGE("error1777");
						return;					
					}
					nth=atoi(pc+4);

					pc=strstr(pc,"e=");
					if(pc==NULL){
						MESSAGE("error1532");
						return;					
					}
					e=atoi(pc+2);

					pc=strstr(pc,"best=");
					if(pc==NULL){
						MESSAGE("error1539");
						return;					
					}
					turn=atoi(pc+5);

				}
				else{
					nth=it->nth;

					for( ; nth>0 ; nth-- ){
						
						e=Solver::estimate( 0,nth==it->nth ? est+1 : MAXN , nth, t, r, layer, 
							nth==NTH ? MDE : nth-1
							,allowFromSetOnly
							);//HAVE TO USE est+1!!
						if(e>0){
							break;
						}
					}
					if(nth==it->nth){
						est-=e;
						if(est<0){
							s="";
							for(i=0;i<layer;i++){
								s1.Format("%04d %d.%d ",t[i],BULL(r[i]),COW(r[i]));
								s+=s1;
							}
							MESSAGE("error1302 id=%d est=%d nth=%d %s",it->id,est,nth,s);
							return;
						}
					}
					if(nth==-1){
						MESSAGE("error1246");
						return;
					}

					if(Solver::getBest()==-1){
						MESSAGE("error1251");
						return;
					}
					turn=Number::array[Solver::getBest()].Int();				
				}
#endif//_MINIMIZE_AVERAGE_

				if(id>=MAX_TREE_NODES){
					MESSAGE("error id>=MAX_TREE_NODES");
					return;
				}

				if(e==0){
					MESSAGE("error e=0");
					return;
				}
				tree[id].set(nth,e,turn,layer,id,i,it,NULL);
				id++;

				it->child[i]=tree+id-1;

			}//for(i=0

		}//for(it



	}//for(layer)
	s1.Format("proceed layer%d [%d %d]\r\n",layer,ec,id-1);
	s+=s1;
	Solver::getOut()->SetWindowText( s );

	//setup estimate for tree[0]
	#ifdef _MINIMIZE_AVERAGE_
	for(e=1,i=0;i<MBC;i++){
		pr=tree[0].child[i];
		if(pr){
			e+=pr->estimate;
		}
	}
	#else
	for(e=i=0;i<MBC;i++){
		pr=tree[0].child[i];
		if(pr && pr->nth==NTH){
			e+=pr->estimate;
		}
	}
	#endif
	tree[0].estimate=e;

	//store tree
	f=fopen("tree"+algS+".txt","w+");
	if(!f){
		MESSAGE("error cann't open file to write");
		return;
	}

	TreeNode::storeHelperString(f,id,algS);

	for(it=tree;it<tree+id;it++){
		it->store(f);
	}
	fclose(f);

	s+="the end";
	Solver::getOut()->SetWindowText( s );
}


void CBcwDlg::test(){
	/*
#ifdef USE_HASH
	int i,j;
	CString s,s1;
	Solver::getOut()->GetWindowText(s);
	HashItem* p=Solver::hashTable;
	for(j=i=0;i<HASH_SIZE;i++,p++){
		j+=p->isNotEmpty();
	}
	s1.Format(" %.2lf%%",j*100./HASH_SIZE);
	s+="hash_fill="+formatNumber(j)+" "+s1;
#ifdef COUNT_HASH_NODES
	s+=" hash_tries="+formatNumber(Solver::hashCounter);
#endif
	s1.Format(" sizeof(HashItem)=%d",sizeof(HashItem));
	s+=s1+"\r\n";
	Solver::getOut()->SetWindowText( s );

#endif
	*/

/*
	CString s,s1;
	int i;
	Number n;
	for(i=0;i<TURN2C;i++){
		n=Number::array[TURN2[i]];
		if(n.onlyDigits0123()){
			continue;
		}
		s1.Format("%d,\r\n",n.Int());
		s+=s1;
	}
	Solver::getOut()->SetWindowText( s );
*/
}


void CBcwDlg::createStringForCrush(){
//#define _STORE_CRUSH5_
//#ifndef _STORE_CRUSH5_ then store crush45
	int i,j,e,turn,mde,es[8];
	bool outR[MBC];
	int t[2]={123};
	int r[2];
	int jobsC=0;
	bool finished;
	CString s,s1,s2,s3,turnsStr;
	FILE*f=fopen("jobs.txt","w+");

	struct TURNS{
		int secondTurn;
		int firstResponse;
		int nth,est;
		int exclude[3];
		TURNS(int _nth,int _firstResponse,int _secondTurn,
			int _exclude0=-1,int _exclude1=-1,int _exclude2=-1){
			nth=_nth;
			secondTurn=_secondTurn;
			firstResponse=_firstResponse;
			exclude[0]=_exclude0;
			exclude[1]=_exclude1;
			exclude[2]=_exclude2;
		}
		inline bool isExclude(int r){
			return std::find(exclude,exclude+3,r)!=exclude+3;
		}
		void setENth(int _est,int _nth){
			est=_est;
			nth=_nth;
		}
		inline bool hasExcludes(){
			return exclude[0]!=-1;
		}
	};
	TURNS turns[]={
		//7th
		TURNS(7,BC01,1245,BC00,BC01,BC02),/*crush45 & crush5*/ //for 2nd response BC00, BC01, BC02 7th>0
#ifdef _STORE_CRUSH5_
		TURNS(7,BC02,1234,BC01,BC02),/*crush5  */ //for 2nd response BC01, BC02 7th>0
#else
		TURNS(7,BC02,1435,BC01,BC02),/*crush45 */ //for 2nd response BC01, BC02 7th>0
#endif

		//6th
		TURNS(6,BC11, 245),
		TURNS(6,BC10, 456),
		TURNS(6,BC00,4567),
		TURNS(6,BC03,1435),
		TURNS(6,BC12, 245),
		TURNS(6,BC20, 245),

		//5th
		TURNS(5,BC21, 456),
		TURNS(5,BC30, 456),
		TURNS(5,BC13,1234),

		//4th
		TURNS(4,BC04,1234),
		TURNS(4,BC22,1234)
	};
	const int szTurns=sizeof(turns)/sizeof(turns[0]);

	const CString BASE_PATH="../bcConsole/results/crush/";

	for(i=0;i<szTurns;i++){
		t[1]=turns[i].secondTurn;
		r[0]=turns[i].firstResponse;
		Solver::subgroups(NULL,outR,t,r,1,0);
		for(j=0;j<8;j++){
			es[j]=0;
		}
		finished=true;
		for(r[1]=0;r[1]<MBC;r[1]++){
			if(outR[r[1]]){
				if(turns[i].isExclude(r[1]) ){
					s2.Format("%d%d-%04d-%d%d-crush%s5.txt"
						,BULL(r[0]),COW(r[0])
						,t[1]
						,BULL(r[1]),COW(r[1])
						,
						#ifdef _STORE_CRUSH5_
						""
						#else
						"4"
						#endif
						);
					s1=BASE_PATH+s2;
					if(checkFileExists(s1)){
						proceedFile(s1,e,turn,mde,true);
						mde--;
						s2.Format("%d",e);
						s3.Format("%04d",turn );
					}
					else{
						MESSAGE("err2041 cann't open "+s1);
						return;
					}
				}
				else{
					s2.Format("exact6/%d%d-%04d-%d%d-exact.txt"
						,BULL(r[0]),COW(r[0])
						,t[1]
						,BULL(r[1]),COW(r[1])
						);
					s1=BASE_PATH+s2;
					if(checkFileExists(s1)){
						proceedFile(s1,e,turn,mde,true);
						mde--;
						s2.Format("%d",e);
						s3.Format("%04d",turn );
					}
					else{
						for(mde=4;mde>0;mde--){
							e=Solver::estimate(0,MAXN,mde+1,t,r,2,mde,false);
							if(e>0){
								break;
							}
						}
						if(e==MAXE){
							s2="?";
							s3="?";
							jobsC++;
							fprintf(f,"solve 0123(%d.%d)+%04d(%d.%d) 6 6 5039#\n",BULL(r[0]),COW(r[0]),
								t[1],BULL(r[1]),COW(r[1]) );
							finished=false;
						}
						else{
							s2.Format("%d",e);
							s3.Format("%04d",Solver::getBest()==-1 ?-1 : Number::array[Solver::getBest()].Int() );
						}
					}
				}
				es[mde+1]+=e;
				


				s1.Format("(%d.%d)+%04d(%d.%d) nth=%d e=%s best=%s\r\n",BULL(r[0]),COW(r[0]),
					t[1],BULL(r[1]),COW(r[1]),mde+1+(e==MAXE),s2,s3);
				s+=s1;
				Solver::getOut()->SetWindowText(s);

				s1.Format("\"$(%d.%d)%04d(%d.%d) nth=%d e=%s best=%s\"\r\n",BULL(r[0]),COW(r[0]),
					t[1],BULL(r[1]),COW(r[1]),mde+1+(e==MAXE),s2,s3);
				turnsStr+=s1;
			}//if(outR
		}//for r[1]
		for(j=7;j>0;j--){
			if(es[j]>0){
				break;
			}
		}
		if( es[j]>=MAXE){
			s2="?";
		}
		else{
			s2.Format("%d",es[j]);
		}

		s1.Format("---  %dth=%s %s----\r\n\r\n"
			,j+(es[j]>=MAXE)
			,s2
			,finished?"":"not finished");
		turns[i].setENth(es[j],j);

		s+=s1;
		Solver::getOut()->SetWindowText(s);
		
	}//for(i)

	for(s2="",i=0;i<szTurns;i++){
		r[0]=turns[i].firstResponse;
		t[1]=turns[i].secondTurn;
		e=turns[i].est;

		s1.Format("\"$(%d.%d) nth=%d e=%d best=%04d\"\r\n",BULL(r[0]),COW(r[0])
			,turns[i].nth
			,e>=MAXE ?-1:e,t[1]);
		s2+=s1;
	}
	turnsStr=s2+turnsStr;

	fclose(f);
	s1.Format("jobsCounter=%d\r\n%s\r\nthe end",jobsC,turnsStr);
	s+=s1;
	Solver::getOut()->SetWindowText(s);
}


const char START_ALPABET='#';
const int MAX_TREE_SYMBOL_CODE=127;
const int RADIX=MAX_TREE_SYMBOL_CODE-START_ALPABET;//not use '\\' so RADIX=MAX_TREE_SYMBOL_CODE-START_ALPABET not RADIX=MAX_TREE_SYMBOL_CODE-START_ALPABET+1
const int RADIX2=RADIX*RADIX;

void CBcwDlg::codeDigitJavaScript(char*c,int n){
	n+=START_ALPABET;
	if(n>='\\'){
	 n++;
	}
	char b[2];
	b[0]=n;
	b[1]=0;
	strcat(c,b);
}

void CBcwDlg::codeNumberJavaScript(char*c,int n,bool oneDigit){
	if(n>=RADIX2){
		MESSAGE("error n>=RADIX2");
		return;
	}
	if(n>=RADIX && oneDigit){
		MESSAGE("error n>=RADIX && oneDigit");
		return;
	}
	if(!oneDigit){
		codeDigitJavaScript(c,n/RADIX);
	}
	codeDigitJavaScript(c,n%RADIX);
}


int CBcwDlg::countInAndCheckSum(TreeNode*t,CPtrTurnType gr,const int cgr,const bool STORE_SUM_TURNS){
	int i,j,k,ind,r=0,sum;
	TurnType g[MAXN];

	if(t->id==0){
		r++;
	}
	for(sum=i=0;i<MBC;i++){
		if(t->child[i]==NULL || t->child[i]->id==0){
			continue;
		}

		if(STORE_SUM_TURNS){
			sum+=t->child[i]->estimate;
		}

		ind=Number::findIndex(t->child[i]->turn);
		if(ind==-1){
			MESSAGE("error ind=-1");
		}
		if(std::find(gr,gr+cgr,ind)!=gr+cgr){
			r++;
		}

		ind=Number::findIndex(t->turn);
		if(ind==-1){
			MESSAGE("error ind=-1");
		}
		for(k=j=0;j<cgr;j++){
			if(RESPONSE(ind,gr[j])==i){
				g[k++]=gr[j];
			}
		}

		r+=countInAndCheckSum(t->child[i],g,k,STORE_SUM_TURNS);
	}

	if(STORE_SUM_TURNS && sum>0){//sum=0 means no children
		if(std::find(gr,gr+cgr,Number::findIndex(t->turn))!=gr+cgr){
			sum+=t->layer+1;
		}
		if(sum!=t->estimate){
			MESSAGE("err671 %d %d %d id=%d\n",sum,t->estimate,t->layer,t->id);
		}
	}
	return r;
}

void CBcwDlg::createJavaScriptStringTree(const char TREE_FILE[],const bool clearJsFile){
	int i,j,k,l,n;
	char*p;
	char b[512];
	FILE*f,*js;
	const char JS_FILE_NAME[]	=	"tree.js";
	const char ITEMS[]				=	"items=";
	const char ALG[]					=	"alg=";
	TreeNode t[MAX_TREE_NODES];
	char algName[128];
	CString s,s1;

	f=fopen(TREE_FILE,"rb");
	if(!f){
		MESSAGE("error cannt open file %s",TREE_FILE);
		return;
	}

	fgets(b,1024,f);
	p=strstr(b,ITEMS);
	if(p==0){
		MESSAGE("error items not found in first line");
		fclose(f);
		return;
	}

	const int treeNodes=atoi(p+sizeof(ITEMS)/sizeof(char)-1);
	if(treeNodes>=MAX_TREE_NODES){
		MESSAGE("error treeNodes>=MAX_TREE_NODES");
		return;
	}

	s1.Format("nodes=%d alg=",treeNodes);
	s+=s1;
	Solver::getOut()->SetWindowText(s);

	p=strstr(b,ALG);
	if(p==0){
		MESSAGE("error alg not found in first line");
		fclose(f);
		return;
	}
	for( i=0,p=p+sizeof(ALG)/sizeof(char)-1 ;  *p!=' ' && *p!='\n' && *p!='\r' ; p++,i++ ){
		algName[i]=tolower(*p);
	}
	algName[i]=0;

	const int algIndex=(algName[5]-'3')*2+(algName[strlen(algName)-1]=='o');

	s+=algName;
	Solver::getOut()->SetWindowText(s);
	//printf("\n");below neew to output js-file-size

	js=fopen(JS_FILE_NAME,clearJsFile?"w+":"a");
	if(js==NULL){
		fclose(f);
		MESSAGE("error cannt open js file");
		return;
	}

	//setup unloaded tree
	for(i=0;i<treeNodes;i++){
		t[i].id=-1;
	}

	//store for javascript
	/*suppose that id goes serially 0,1,2,3,
	1st store item with id=0,2nd item with id=1 ..., so id parameter can be skipped


	use 92 base system from #(code35) to del(code127) except backslash(code92), radix=127-35+1-1=92
	also use symbols {!,space,"} see below.
	We assume that treeSize<=8464 items(now it's size about 5500), so any digit we can encode up to two symbols.

	
	we init a long string of inititalization
	use '!'as items separator, use ' ' as params separator,use quotes for string o="..."

	for turns use undexes in Array in c++ or gArray in js
	o="0 1 2 3 4 5 6 7 8 9 a b c d!8e0 0 0 e f 10 0 11 12 13 14 15 16 17!"
	o="turnIndex childId[0] childId[1] ....!"
	or alternative o="turnIndex k_1 childId[k_1] k_2 childId[k_2]...!"
	*/

	fprintf(js,"%c\"",clearJsFile?' ':',');

//	id layer nth estimate turn parentId child[] items=4139 alg=crush35

	int id,layer,nth,estimate,turn,parentId,chId[MBC],r=-1;
	while(fgets(b,1024,f)!=0){
		j=sscanf(b,"%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
			&id,&layer,&nth,&estimate,&turn,&parentId,
			chId+0,
			chId+1,
			chId+2,
			chId+3,
			chId+4,
			chId+5,
			chId+6,
			chId+7,
			chId+8,
			chId+9,
			chId+10,
			chId+11,
			chId+12,
			chId+13,
			chId+14,
			chId+15
		);

		TreeNode* child[MBC];
		for(i=0;i<MBC;i++){
			child[i]=(j==6?t:t+chId[i]);
		}

		if(turn==-1){
			MESSAGE("error 2215");
			return;
		}
		if(estimate==0 && (algName[strlen(algName)-1]=='o' || algName[strlen(algName)-1]!='o' && nth!=7) ){
			MESSAGE("error 2219");
			return;
		}


		t[id].set(nth,estimate,turn,layer,id,0,t+parentId,child);
		//t[id].setRecent();
		if(layer>r){
			r=layer;
		}


		i=Number::findIndex(turn);
		if(i==-1){
			MESSAGE("error 300");
			fclose(f);
			fclose(js);
			return;
		}

		if(j!=6 && t[id].needStore()){
			b[0]=0;
			codeNumberJavaScript(b,i,false);
			fprintf(js,"%s",b);
		}
	}
	fprintf(js,"\"\n");
	i=ftell(js);
	//tree loaded

	s+=" js_file_size="+formatNumber(i)+"\r\n";
	Solver::getOut()->SetWindowText(s);

	fclose(f);
	fclose(js);

	const int MT=r+2;
	const int MAXR=BC40+1;
	const int STAT_SIZE=MAXR*MT;

	short* stat=new short[STAT_SIZE];
	for(i=0;i<STAT_SIZE;i++){
		stat[i]=0;
	}

	TreeNode*pt;
	//find how many nodes in set
	TurnType gr[MAXN];
	for(i=0;i<MAXN;i++){
		gr[i]=i;
	}

	i=countInAndCheckSum(t,gr,MAXN,t[0].estimate>MAXN);
	s1.Format("nodes in tree %d/%d %.2lf%%\r\n",i,treeNodes,i*100./treeNodes);
	s+=s1;
	Solver::getOut()->SetWindowText(s);

	const int index0=Number::findIndex(t[0].turn);

	for(i=0;i<MAXN;i++){//solve number "i"
		for( pt=t,layer=1 ;  ; layer++ ){
			j=Number::findIndex(pt->turn);
			if(j==-1){
				MESSAGE("Number::findIndex error");
				delete[]stat;
				return;
			}
			r=RESPONSE(j,i);
			if(r==BC40){
				stat[RESPONSE(i,index0)*MT+layer]++;
				break;
			}
			pt=pt->child[r];
			
			if(layer>MT){
				MESSAGE("error186");
				delete[]stat;
				return;
			}
		}			
	}

	s1.Format("statistics\r\n");
	s+=s1;
	for(k=j=0,i=1;i<MT;i++){
		for(n=l=0;l<MAXR;l++){
			n+=stat[l*MT+i];
		}
		s1.Format("%d %4d\r\n",i,n);
		s+=s1;
		j+=n;
		k+=i*n;
	}
	if(j!=MAXN){
		s1.Format("WARNING total %d!=MAXN\r\n",j);
		s+=s1;
	}

	s+="total="+formatNumber(k);
	s1.Format(" average=%.4lf\r\n",double(k)/MAXN);
	s+=s1;

	s1="";
	appendCharsToLength(s1,66,'-');
	s+=s1+"\r\n  ";

	for(r=0;r<MBC;r++){
		if(findEnumResponse(r)==-1){
			continue;
		}
		s1.Format(" %d.%d ",BULL(r),COW(r));
		s+=s1;
	}
	s+="\r\n";
	for(i=1;i<MT;i++){
		s1.Format("%d",i);
		s+=s1;
		for(r=0;r<MBC;r++){
			if(findEnumResponse(r)==-1){
				continue;
			}
			if(stat[r*MT+i]!=0){
				s1.Format("%5d",stat[r*MT+i]);
				s+=s1;
			}
			else{
				s1="";
				appendCharsToLength(s1,5);
				s+=s1;
				//s+="    ";
			}
		}
		s+="\r\n";
	}

	for(j=i=0;i<STAT_SIZE;i++){
		j+=stat[i];
	}
	if(j!=MAXN){
		s1.Format("Error sum(stat)!=MAXN %d %d",j,MAXN);
		s+=s1+"\r\n";
	}

	delete[]stat;

	s+="the end";
	Solver::getOut()->SetWindowText(s);

}

void CBcwDlg::createJavaScriptStringTreeAll(){
	int i;
	const CString TREE_FILE[]	=	{
	#ifdef _BULLS_COWS_
		"Crush5o",
		"Avg5",
	#else
		"CrushMastermind",
		"AvgMastermind5",
		"AvgMastermind",
	#endif
	};
	const int MAX_TREE=sizeof(TREE_FILE)/sizeof(TREE_FILE[0]);

	char c[32];

	for(i=0;i<MAX_TREE;i++){
		sprintf(c,"tree%s.txt",TREE_FILE[i]);
		createJavaScriptStringTree(c,
	#ifdef _BULLS_COWS_
			i==0
	#else
			false
	#endif
			);
	}
}

int CBcwDlg::findEnumResponse(int r){
	int i;
	for(i=0;i<szEnumResponse;i++){
		if(EnumResponse[i]==r){
			return i;
		}
	}
	return -1;
}

const CString CBcwDlg::getShortAlgorithmName(bool capitilizeFirstChar){
	char s[]=
	#ifdef _MINIMIZE_AVERAGE_
	"avg"
	#else
	"crush"
	#endif
	;

	if(capitilizeFirstChar){
		s[0]=toupper(s[0]);
	}

	return s;
}

CString CBcwDlg::getAlgorithmString(int mde,int count,int length){
	CString s;
	if(mde+2==count){
		s="exact";
	}
	else{
		if(mde+1>=count-2){
			s.Format("%s%d",getShortAlgorithmName(),mde+1);
		}
		else{
			s.Format("%s%d%d",getShortAlgorithmName(),mde+1,count-2);
		}
	}
	appendCharsToLength(s,length);
	return s;
}

const CString CBcwDlg::getHeuristicAlgorithmName(){
	return
	#ifdef _LARMOUTH_
	"larmouth"
	#else
	"crush"
	#endif
	;
}

const CString CBcwDlg::formatNumber(int n){
	CString s;
	if(n<1000){
		s.Format("%d",n);
	}
	else if(n<1000000){
		s.Format("%d %03d",n/1000,n%1000);
	}
	else{
		s.Format("%d %06d",n/1000000,n%1000000);
	}
	return s;
}

void CBcwDlg::appendCharsToLength(CString&s,const int length,const char add){
	while(s.GetLength()<length){
		s+=add;
	}
}

void MESSAGE(const char* format, ...){
  va_list argptr;
  va_start(argptr, format);
	char c[1024];
  vsprintf(c, format, argptr);
  va_end(argptr);
	CWnd*p=AfxGetMainWnd();
	p->SetWindowText("message|error");
	p->MessageBox(c,"message");
}
