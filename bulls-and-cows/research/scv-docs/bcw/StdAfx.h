// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__0A784017_1B83_4622_93FB_F9B5389736DB__INCLUDED_)
#define AFX_STDAFX_H__0A784017_1B83_4622_93FB_F9B5389736DB__INCLUDED_

/*
BASIC #define(s)
//_BULLS_COWS_ & _MINIMIZE_AVERAGE_ define game type & optimization criterion
//_UNIX _DEBUG _CONSOLE
*/

//ifdef _BULLS_COWS_ bulls and cows game otherwise mastermind game(6 pegs or digits,equal digits is possible)
//#define _BULLS_COWS_


//ifdef  _MINIMIZE_AVERAGE_ - minimize average amount of turns to guess secret number, otherwise minimize 7th
//#define _MINIMIZE_AVERAGE_



//BELOW AUTOMATICALLY DEFINED CONSTANTS & OTHERS

//use hash table for searching
#define USE_HASH

//larmouth strategy(uses for minimize game length), otherwise crush(uses for minimize amount of turns with max turn guess) algorithm is used
#if !defined(_LARMOUTH_ ) && defined(_MINIMIZE_AVERAGE_)
	#define _LARMOUTH_ 
#endif


#ifdef _UNIX
	#define _CONSOLE
  #include <string.h>
#endif


#ifdef _BULLS_COWS_
	const int DIGITS=10;
	const int MAXN = 5040;
#else
	const int DIGITS=6;
	#define _ALLOW_SAME_DIGITS_
	const int MAXN = 1296;
#endif

const int POSITIONS=4;//NOTE Code is debugged only for POSITIONS=4 (only Transposition class is adopted)

#define BC(b,c) (b)*(POSITIONS+1)+(c)
#define BULL(r) (r)/(POSITIONS+1)
#define COW(r)  (r)%(POSITIONS+1)

const int MBC  = BC(POSITIONS-1,0)+1;

#ifdef _ALLOW_SAME_DIGITS_
	const bool ALLOW_SAME_DIGITS=true;
#else
	const bool ALLOW_SAME_DIGITS=false;
#endif


#ifdef  _MINIMIZE_AVERAGE_
	const int MAXE=(1<<15)-1;// for hash use max 15bits only
#else
	const int MAXE=MAXN;
#endif


#ifdef _CONSOLE
	#define MESSAGE printf
#else
	void MESSAGE(const char* format, ...);
#endif

const int BC00=BC(0,0);
const int BC01=BC(0,1);
const int BC02=BC(0,2);
const int BC03=BC(0,3);
const int BC04=BC(0,4);
const int BC10=BC(1,0);
const int BC11=BC(1,1);
const int BC12=BC(1,2);
const int BC13=BC(1,3);
const int BC20=BC(2,0);
const int BC21=BC(2,1);
const int BC22=BC(2,2);
const int BC30=BC(3,0);
const int BC40=BC(4,0);

const int szEnumResponse=14;
const int EnumResponse[szEnumResponse]={
	BC00, BC01,	BC02,	BC03,	BC04,
	BC10,	BC11,	BC12,	BC13,
	BC20,	BC21,	BC22,
	BC30,
	BC40
};

typedef short								TurnType;
typedef const TurnType			CTurnType;
typedef TurnType*						PtrTurnType;
typedef TurnType const*			CPtrTurnType;


#pragma comment(linker, "/ignore:4089") 

#pragma warning( disable : 4786 )
#include <math.h>
#include <algorithm>
#include <functional>
#include <vector>

#ifdef _CONSOLE
	#include <stdio.h>
	#include <time.h>

#else

	#if _MSC_VER > 1000
	#pragma once
	#endif // _MSC_VER > 1000

	#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

	#ifndef _AFX_NO_AFXCMN_SUPPORT
	#include <afxcmn.h>			// MFC support for Windows Common Controls
	#endif // _AFX_NO_AFXCMN_SUPPORT

#endif // _CONSOLE

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__0A784017_1B83_4622_93FB_F9B5389736DB__INCLUDED_)
