/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#include "StdAfx.h"
#include "treeNode.h"
#include "number.h"

void TreeNode::store(FILE*f){
	int i;
	//not store "response" member
	fprintf(f,"%5d %1d %1d %4d %4d %5d ",id,layer,nth,estimate,turn,parent==NULL?0:parent->id);

	if(hasChildren()){
		for(i=0;i<MBC;i++){
			fprintf(f," %d",child[i]==NULL?0:child[i]->id);
		}
	}
	fprintf(f,"\n");
}

void TreeNode::storeHelperString(FILE*f,int nItems, const char* algorithmName){
	fprintf(f,"id layer minTurns estimate turn parentId child[] items=%d alg=%s\n",nItems,algorithmName);
}

bool TreeNode::hasChildren()const{
	int i;
	for(i=0;i<MBC;i++){
		if(child[i]!=NULL){
			return true;
		}
	}
	return false;
}

void TreeNode::set(int _nth,int _estimate,int _turn,int _layer, int _id, int _response, 
									 TreeNode* _parent,TreeNode** _child){
	int i;

	nth=_nth;
	estimate=_estimate;
	turn=_turn;
	layer=_layer;
	id=_id;
	response=_response;
	parent=_parent;

	if(_child!=NULL){
		for(i=0;i<MBC;i++){
			child[i]=_child[i];
		}
	}
	else{
		for(i=0;i<MBC;i++){
			child[i]=0;
		}
	}

}

bool TreeNode::needStore()const{
	TreeNode const* p,*pp;
	int i,j,c=0,n;
	int t[7],r[7],re[szEnumResponse];
	bool q[MBC];

	for( n=0,p=this ; p->id!=0 ; p=p->parent ){
		pp=p->parent;
		for(i=0;i<MBC;i++){
			if(pp->child[i]==p){
				break;
			}
		}
		t[n]=Number::findIndex(pp->turn);
		r[n]=i;
		n++;
	}

	for(j=0;j<MAXN;j++){
		for(i=0;i<n;i++){
			if(RESPONSE(t[i],j)!=r[i]){
				break;
			}
		}
		if(i==n){
			if(c>=szEnumResponse){
				return true;
			}
			re[c++]=j;
		}
	}
	if(c<=2){
		return false;
	}
	for(j=0;j<c;j++){
		for(i=0;i<MBC;i++){
			q[i]=false;
		}
		for(i=0;i<c;i++){
			if(i==j){
				continue;
			}
			n=RESPONSE(re[j],re[i]);
			if(q[n]){
				break;
			}
			q[n]=true;
		}
		if(i==c){
			return false;
		}
	}
	return true;
}
