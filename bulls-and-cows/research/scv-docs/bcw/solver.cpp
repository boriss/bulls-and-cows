/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/

#include "StdAfx.h"
#include "solver.h"
#include "turns.h"
#include "number.h"
#include "numbers_group.h"
#include "hash.h"

#ifdef COUNT_NODES
	unsigned int Solver::nodes=0;
#endif

#ifndef _CONSOLE
 #include "resource.h"

 #include "bcwDlg.h"
 extern CBcwDlg* dlg;

 CWnd*		Solver::sOut[5];

#endif

extern Number Array[MAXN];

Solver	Solver::p[8];
int			Solver::turn[7];

clock_t Solver::sTimeStart;
int			Solver::sNCount;
int			Solver::sOutDepth;
int			Solver::sDepthExact;

#ifdef USE_HASH
HashItem*Solver::hashTable;
#ifdef COUNT_HASH_NODES
unsigned Solver::hashCounter=0;
#endif
#endif


bool Solver::subgroups(char*out,bool*outR,const int* t,const int* r, int ct,int par){
	char s[4096];
	bool result=false;

	Solver& p0=p[0];
	Solver& p1=p[1];

	int i,j;
	int sit[7];

	for(i=0;i<=ct;i++){
		sit[i]=Number::findIndex(t[i]);
		if(sit[i]==-1){
			MESSAGE("ERROR Solver::subgroups INDEX NOT FOUND!");
			throw 0;
		}
	}

	for(p0.cq[0]=i=0;i<MAXN;i++){
		for(j=0;j<ct;j++){
			if( RESPONSE(sit[j],i)!=r[j]){	
				int k=RESPONSE(sit[j],i);
				break;
			}
		}
		if(j==ct){
			p0.q[0][ p0.cq[0]++ ]=i;
		}
	}

	if(out){
		if(par==0){
			sprintf(out,"items=%d ",p0.cq[0]);
		}
		else if(par==1){
			sprintf(out,"solving ");
		}
		for(i=0;i<ct;i++){
			sprintf(s,"%04d(%d.%d)+",t[i],BULL(r[i]),COW(r[i]));
			strcat(out,s);
		}

		sprintf(s,"%04d(*.*)\n",t[ct]);
		strcat(out,s);
	}

	for(i=0;i<MBC;i++){
		p1.cq[i]=0;
	}

	for(i=0;i<MBC;i++){
		outR[i]=false;
	}

	for(i=0;i<p0.cq[0];i++){
		j=RESPONSE(p0.q[0][i],sit[ct]);
		if(j==BC40){
			result=true;
			j=BC(1,POSITIONS);//NOTE j=BC(1,POSITIONS)<MBC so index in range
			if(par==2){
				outR[j]=true;	
			}
		}
		else{
			outR[j]=true;
		}
	
		p1.cq[j]++;

	}

	if(out){
		for(i=0;i<MBC;i++){
			j = i==BC(1,POSITIONS) ? BC40:i;
			if(p1.cq[i]>0){
				sprintf(s,"%d,%d %3d\n",BULL(j),COW(j),p1.cq[i]);
				strcat(out,s);
			}
		}
	}
	return result;
}

void Solver::init(){
	int i;
	for(i=0;i<sizeof(p)/sizeof(p[0]);i++){
		p[i].prev= i==0 ? 0 : p+i-1;
		p[i].next= (i+1==sizeof(p)/sizeof(p[0])) ? 0 : p+i+1;
		p[i].depth=i;
		p[i].split=new GroupsSet[MAXN];
	}
#ifdef USE_HASH
	hashTable=new HashItem[HASH_SIZE];
#endif


#ifndef _CONSOLE
	UINT id[]={IDC_EDIT_BIG,IDC_EDIT1,IDC_EDIT2,IDC_EDIT3,IDC_EDIT4};
	for(i=0;i<5;i++){
		sOut[i]=dlg->GetDlgItem(id[i]);
	}
#endif
}

void Solver::deInit(){
	int i;
	for(i=0;i<sizeof(p)/sizeof(p[0]);i++){
		delete [](p[i].split);
	}
#ifdef USE_HASH
	delete[]hashTable;
#endif
}

int Solver::estimate(const int alpha,int beta,const int depthCount,const int* t,const int* r,const int ct
										 ,const int mde,const bool allowFromSetOnly){
	int i,j;

	sTimeStart=clock();
	sOutDepth = ct+1;

	sNCount=depthCount-1;//use -1
	sDepthExact=mde;

	for(i=0;i<ct;i++){
		turn[i]=Number::findIndex(t[i]);
	}

	for(p[ct].cq[0]=0,i=0;i<MAXN;i++){
		for(j=0;j<ct;j++){
			if( RESPONSE(turn[j],i)!=r[j] ){
				break;
			}
		}
		if(j==ct){
			p[ct].q[0][ p[ct].cq[0]++ ]=i;
		}
	}

	//need to setup turnCount!!!!!!!!!!!
#ifdef _ALLOW_SAME_DIGITS_
	//check t[0] in TURN1 && t[1] in TURN2[t[0]]
	if(ct==1 || ct==2){
		if((i=Number::firstDifferentTurnIndex(t[0]))==-1){
			MESSAGE("ERROR TURN1 INDEX NOT FOUND! 182");
			throw 0;
		}
		else{
			p[1].turnCount=i;
		}
	}

	if(ct==2){
		if((i=Number::secondDifferentTurnIndex(p[1].turnCount,t[1]))==-1){
			MESSAGE("ERROR TURN1 INDEX NOT FOUND! 182");
			throw 0;
		}
		else{
			p[2].turnCount=i;
		}
	}
#else
	if(ct==2){//sOutDepth=3
		if((i=Number::firstDifferentTurnIndex(t[ct-1]))==-1){
			MESSAGE("ERROR TURN2 INDEX NOT FOUND! 170");
			throw 0;
		}
		else{
			p[sOutDepth-1].turnCount=i;
		}
	}
#endif

	return p[sOutDepth].solve(alpha,beta,p[sOutDepth-1].q[0],p[sOutDepth-1].cq[0],allowFromSetOnly);

}

int Solver::solve(int alpha,int beta,PtrTurnType gr,const int cgr,const bool allowFromSetOnly){
#ifdef COUNT_NODES
	nodes++;
#endif
	int i,t,r,e,lo;
	CPtrTurnType pt;
	bool maxSizeOne,fromSet,fromSetOnly=false;

#ifdef USE_HASH
	char hashFlag=HASH_BETA;
#endif

	best=-1;
#define _SOLVE_
	#include "prealg.h"
#undef _SOLVE_

	//equivalent split
	PtrGroupsSet pSplit=split,ps,*pps;
	const bool useSplitCutoffs= depth>
#ifdef _ALLOW_SAME_DIGITS_
	2
#else
	3
#endif
	;
	if(useSplitCutoffs){
		for(i=0;i<GROUPSSET_HASH_SIZE;i++){
			hashSplit[i]=NULL;
		}
	}

#ifdef USE_HASH
	if( (i=checkHash(gr,cgr,beta,allowFromSetOnly,false))!=-1){
		return i;
	}
#endif

	for(turnCount=0;*pt!=-1;pt++,turnCount++){
		t=*pt;


		for(i=0;i<MBC;i++){
			cq[i]=0;
		}
		
		e=0;

		//split gr into q with turn t
		for(fromSet=false,i=0;i<cgr;i++){
			r=RESPONSE(t,gr[i]);
			if( r==BC40 ){
				fromSet=true;
				#ifdef  _MINIMIZE_AVERAGE_
				e+=depth;
				#endif
				continue;
			}
			q[r][ cq[r]++ ] = gr[i];
			if(cq[r]>=cgr){
				goto nextpt;
			}

		}
		if(fromSetOnly && !fromSet){
			continue;
		}

		//equivalent split
		if(useSplitCutoffs){
			pSplit->init(q,cq,fromSet);
			pps=hashSplit+pSplit->hash();

			//try to find equal group
			for(ps=*pps;ps!=NULL;ps=ps->next){
				if(*pSplit==*ps){
					break;
				}
			}
			if(ps==NULL){//equal not found
				pSplit->next=*pps;
				*pps=pSplit;
				pSplit++;
			}
			else{//skip this turn
				continue;
			}
		}

		for(maxSizeOne=true,i=0;i<MBC;i++){
			if(depth==sNCount-1){//prelast layer
				lo=e;
				for(r=i;r<MBC;r++){
					lo+=
					#ifdef  _MINIMIZE_AVERAGE_
						//depth+1+(depth+2)*(cq[i]-1) = (depth+2)-1+(depth+2)*(cq[i]-1)=(depth+2)*cq[i]-1
						(depth+2)*cq[r]-1
					#else
						cq[r]-1
					#endif
						;
				}
				if(lo>=beta){
					goto nextpt;
				}
			}
			
			#ifdef  _MINIMIZE_AVERAGE_
			if( cq[i]==1 ){
				e+=depth+1;
			}else
			#endif
			if( cq[i]>1 ){
				maxSizeOne=false;

				e+=next->solve(0,beta-e,q[i],cq[i],allowFromSetOnly);

				if(e>=beta){
					break;
				}

			}
		}
		if(e<=alpha){
			best=t;
			return e;
		}
		if(e<beta){

			beta=e;
			best=t;
#ifdef USE_HASH
			hashFlag=HASH_EXACT;
#endif

			if(maxSizeOne && !fromSet){
				fromSetOnly=true;
			}
			if(alpha>=beta){
				break;
			}

			#ifndef  _MINIMIZE_AVERAGE_
			if(maxSizeOne){
				beta=0;
				break;
			}
			#endif

			if( (fromSet && maxSizeOne) || beta==
				#ifdef  _MINIMIZE_AVERAGE_
				cgr*(depth+1)-1
				#else
				0
				#endif
				){
				break;//to store hash and or/tree
			}

		}
nextpt:;
	}//for(*pt)

#ifdef USE_HASH
	fillHash(beta,hashFlag);
#endif
	return beta;
}

int Solver::solveHeuristic(int alpha,int beta,PtrTurnType gr,const int cgr,const bool allowFromSetOnly){
#ifdef COUNT_NODES
	nodes++;
#endif

	int i,t,r,e;
	bool fromSet;
	CPtrTurnType pt;
	#include "prealg.h"

#ifdef USE_HASH
	if((i=checkHash(gr,cgr,beta,allowFromSetOnly,true))!=-1){
		return i;
	}
#endif


#ifdef _LARMOUTH_ //solveLarmouth

	double bestV=99999999999,v;

	for(;*pt!=-1;pt++,turnCount++){
		t=*pt;

		for(i=0;i<MBC;i++){
			cq[i]=0;
		}
		fromSet=false;

		for(i=0;i<cgr;i++){
			r=RESPONSE(t,gr[i]);
			if( r==BC40 ){
				fromSet=true;
				continue;
			}

			cq[r]++;
			if(cq[r]>=cgr){
				goto nextpt;
			}


		}//for(i)


		for(v=i=0;i<MBC;i++){
			if(cq[i]>1){
				v+=cq[i]*log(cq[i]);
			}
		}
		if(fromSet){
			v-=2*log(2);
		}
		if(v<bestV){
			bestV=v;
			best=t;
		}
nextpt:;
	}//for(pt)

#else // _LARMOUTH_
	bool fromSetOnly=false;
	int _best[MBC]={cgr};

	for(;*pt!=-1;pt++,turnCount++){
		t=*pt;

		if(fromSetOnly && std::find(gr,gr+cgr,t)==gr+cgr ){
			continue;
		}
		
		for(i=0;i<MBC;i++){
			cq[i]=0;
		}
		fromSet=false;

		for(i=0;i<cgr;i++){
			r=RESPONSE(t,gr[i]);
			if( r==BC40 ){
				fromSet=true;
				continue;
			}

			cq[r]++;

			if(cq[r]>_best[0]){
				goto nextpt;
			}

		}//for(i)

		std::sort(cq,cq+MBC,std::greater<int>());

		for(i=0;i<MBC && _best[i]==cq[i];i++);

		if(i==MBC){
			if(fromSet){
				best=t;
			}
		}
		else if(cq[i]<_best[i]){
			for(i=0;i<MBC;i++){
				_best[i]=cq[i];
				best=t;
			}
			if(cq[0]==1){
				if(fromSet){
					break;
				}
				else{
					fromSetOnly=true;
				}
			}
		}
nextpt:;
	}//for(pt)
#endif //_LARMOUTH_

	for(i=0;i<MBC;i++){
		cq[i]=0;
	}

	e=0;
	for(i=0;i<cgr;i++){
		r=RESPONSE(best,gr[i]);
		if( r==BC40 ){
			#ifdef  _MINIMIZE_AVERAGE_
			e=depth;
			#endif
			continue;
		}

		q[r][ cq[r]++ ] = gr[i];
	}

	//"e" already setup
	for(i=0;i<MBC;i++){
		#ifdef  _MINIMIZE_AVERAGE_
		if( cq[i]==1 ){
			e+=depth+1;
		}else
		#endif
		if( cq[i]>1 ){
			e+=next->solveHeuristic(alpha,beta-e,q[i],cq[i],allowFromSetOnly);
			if(e>=beta){
				break;
			}
		}
	}

#ifdef USE_HASH
	fillHash( e<beta ? e : beta , e<beta ? HASH_EXACT : HASH_BETA );
#endif
	return e;
}

int Solver::solveLast(int alpha,int beta,CPtrTurnType gr,const int cgr,const bool allowFromSetOnly){
//this node already counted
//#ifdef COUNT_NODES
//	nodes++;
//#endif
	int i,t,r;
	if(cgr>szEnumResponse){
		return MAXE;
	}

	if(
		#ifdef  _MINIMIZE_AVERAGE_
		cgr*(depth+1)-1
		#else
		cgr-1
		#endif
		>=beta){
		return beta;
	}


	//1st try turns from group
	for(t=0;t<cgr;t++){
		for(i=0;i<MBC;i++){
			cq[i]=0;
		}

		for(i=0;i<cgr;i++){
			r=RESPONSE(gr[t],gr[i]);//here gr[t]
			if(r==BC40){
				continue;
			}
			cq[r]++;
			if(cq[r]>1){
				break;
			}
		}//for(i)

		if(i==cgr){
			best=gr[t];
			#ifdef  _MINIMIZE_AVERAGE_
			return cgr*(depth+1)-1;//(cgr-1)*(depth+1)+depth;
			#else
			return cgr-1;
			#endif
		}			
	}//for(t)


	if(cgr==szEnumResponse){
		return MAXE;
	}

	if(
		#ifdef  _MINIMIZE_AVERAGE_
		cgr*(depth+1)
		#else
		cgr
		#endif
		>=beta){
		return beta;
	}

#ifdef USE_HASH
	setIn(gr,cgr);
	if( (i=checkHash(gr,cgr,beta,allowFromSetOnly,false))!=-1){
		return i;
	}
#endif

	if(!allowFromSetOnly){
		//2nd try all other turns
#ifndef USE_HASH
		setIn(gr,cgr);
#endif


		for(t=MAXN-1;t>=MIN_TURN_NUMBER;t--){
			if(in[t]==0){

				for(i=0;i<MBC;i++){
					cq[i]=0;
				}

				for(i=0;i<cgr;i++){
					r=RESPONSE(t,gr[i]);
					if(r==BC40){
						continue;
					}
					cq[r]++;
					if(cq[r]>1){
						break;
					}
				}//for(i)

				if(i==cgr){
					best=t;
					i=cgr
					#ifdef  _MINIMIZE_AVERAGE_
					*(depth+1)
					#endif
					;
#ifdef USE_HASH
					fillHash(i,HASH_EXACT);
#endif
					return i;
				}			
			}
		}//for(t)
	}

#ifdef USE_HASH
	fillHash(MAXE,HASH_EXACT);
#endif
	return MAXE;
}

int Solver::getBest(){
	return p[Solver::sOutDepth].best;
}

CPtrTurnType Solver::reorderTurns(CPtrTurnType pt,CPtrTurnType gr,const int cgr){
	int i,t;
	CPtrTurnType p;
	for(i=0;i<cgr;i++){
		orderedTurns[i]=gr[i];
	}

	if(pt==NULL){
		for(t=MAXN-1;t>=MIN_TURN_NUMBER;t--){
			if(in[t]==0){
				orderedTurns[i++]=t;
			}
		}
	}
	else{
		for(p=pt;*p!=-1;p++){
			t=*p;
			if(in[t]==0){
				orderedTurns[i++]=t;
			}
		}	
	}
	orderedTurns[i]=-1;
	pt=orderedTurns;
	return pt;
}

void Solver::setIn(CPtrTurnType gr,const int cgr){
	int i;
	for(i=0;i<MAXN;i++){
		in[i]=0;
	}
	for(i=0;i<cgr;i++){
		in[gr[i]]=1;
	}
}

#ifdef USE_HASH
void Solver::fillHash(int estimate,char flag){
#ifdef COUNT_HASH_NODES
	hashCounter++;
#endif
	hashItem.set(estimate, depth, best, flag);
	hashTable[hashItem.hash()]=hashItem;
}
#endif


#ifdef USE_HASH
int Solver::checkHash(CPtrTurnType gr,const int cgr,int beta,bool allowFromSetOnly,bool heuristic){
	hashItem.set(in,sDepthExact,allowFromSetOnly,heuristic);
	const HashItem* pHashItem=hashTable+hashItem.hash();
	if(*pHashItem==hashItem){
		best=pHashItem->turn;
		if(pHashItem->flag==HASH_EXACT){
			if(depth==pHashItem->depth){
				return pHashItem->estimate;
			}
			else if(depth < pHashItem->depth){
				if(pHashItem->estimate!=MAXE){
					#ifdef  _MINIMIZE_AVERAGE_
					return pHashItem->estimate-cgr*(pHashItem->depth-depth);
					#else
					return 0;
					#endif
				}
			}
			else{//depth>pHashItem->depth
				if(pHashItem->estimate>0){
					return MAXE;
				}
			}
		}
		else{// >=beta
			if(depth==pHashItem->depth){
				if(beta<=pHashItem->estimate){
					return beta;
				}
			}
			else if(depth > pHashItem->depth){
				if(pHashItem->estimate>0){
					return MAXE;
				}
			}
#ifdef  _MINIMIZE_AVERAGE_
			else{//depth < pHashItem->depth
				if(beta<= pHashItem->estimate-cgr*(pHashItem->depth-depth) ){
					return beta;
				}
			}
#endif
		}
	}
	return -1;
}
#endif



