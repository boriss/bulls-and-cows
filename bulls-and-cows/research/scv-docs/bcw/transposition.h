/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#ifndef _transposition_
#define _transposition_

class Transposition{

	int d[DIGITS];
	bool id[DIGITS];//inverted d(true if already set)
	int p[POSITIONS];

	static Transposition* POSITION_TRANSFORM;
	static int POSITION_TRANSFORM_SIZE;

	friend void transposition_hf(int*,int);

	void getIndex(int index,int*iv,int*niv)const;

	//t[tc] - array of indexes of Array
	bool leaveSame(int const* t,int tc,int p,int q);

	//return true if transposition exists. See isEquivalent function
	bool transpositionExists(int*o,int*n);

public:

	Transposition(){
	}

	static void init();
	static void deInit();

	/*t[tc] - array of indexes of Array
	return true is exists \psi:
	\psi(Array[t[i]])=Array[t[i]] for every i=0...tc-1
	and \psi(Array[p])=Array[q]

	t can be NULL, with tc=0
	*/
	static bool isEquivalent(int const* t,int tc,int p,int q);
};

#endif