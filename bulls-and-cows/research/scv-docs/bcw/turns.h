/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#ifndef _TURNS_
#define _TURNS_

//for gametype definition
#include "stdafx.h"

#ifdef _BULLS_COWS_
	#include "bullscows_turns.h"
#else
	#include "mastermind_turns.h"
#endif

#endif