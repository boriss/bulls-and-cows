//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by bcw.rc
//
#define IDD_BCW_DIALOG                  102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_START                1000
#define IDC_EDIT1                       1002
#define IDC_EDIT2                       1003
#define IDC_EDIT3                       1004
#define IDC_EDIT4                       1005
#define IDC_STATUS                      1006
#define IDC_BUTTON_IMMEDIATE            1007
#define IDC_BUTTON_STORE                1007
#define IDC_EDIT_BIG                    1009
#define IDC_BUTTON_BETTER_FROM_OUT      1010
#define IDC_BUTTON_TEST                 1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
