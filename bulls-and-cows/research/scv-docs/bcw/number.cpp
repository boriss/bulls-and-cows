/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2008-doomsday
email slovesnov@yandex.ru
homepage http://slovesnov.users.sourceforge.net
*****************************************************************************************************************/
#include "StdAfx.h"
#include "number.h"
#include "solver.h"
#include "Permutations.h"
#include "turns.h"

Number Number::array[MAXN];

char* Number::RA=0;

void Number::init(){
	int j,k;
	Number *pArray=array;
	Permutations permutation(POSITIONS,DIGITS,
#ifdef _ALLOW_SAME_DIGITS_
		PERMUTATIONS_WITH_REPLACEMENTS
#else
		PERMUTATIONS_WITHOUT_REPLACEMENTS
#endif
	);
	do{
		for(j=0;j<permutation.getK();j++){
			pArray->n[j]=permutation.getIndex(j);
		}
		pArray++;
	}while(permutation.next());


  RA=new char[MAXN*MAXN];
  if(!RA){
		MESSAGE("error couldn't allocate the memory");
	}

  for(k=0;k<MAXN;k++){
    for(j=k;j<MAXN;j++){
      RESPONSE(k,j)=RESPONSE(j,k)=array[k].getResponse(array[j]);
		}
	}
}

void Number::deInit(){
	if(RA!=NULL){
		delete[]RA;
	}
}

int	Number::getResponse(Number const x)const{
	int i;
	int a[DIGITS];
	for(i=0;i<DIGITS;i++){
		a[i]=0;
	}

	int r=0;

	for(i=0;i<POSITIONS;i++){
		if(n[i]==x.n[i]){
			r+=POSITIONS+1;
		}
		else{
			if( a[n[i]]++ <0 ){
				r++;
			}
			if( a[x.n[i]]-- >0 ){
				r++;
			}
		}
	}

	return r;
}



//find index in array[i]
int Number::findIndex(int n){
	for(int i=0;i<MAXN;i++){
		if(array[i].Int()==n){
			return i;
		}
	}
	return -1;
}

bool Number::onlyDigits0123()const{
	for(int i=0;i<4;i++){
		if(n[i]<0 || n[i]>3){
			return false;
		}
	}
	return true;

}

CPtrTurnType Number::firstDifferentTurnPtr(int turn){
	int i=firstDifferentTurnIndex(turn);
	return i==-1?0:DIFFERENT_TURN1+i;
}

CPtrTurnType Number::secondDifferentTurnPtr(int firstTurnIndex,int turn){
	int i=secondDifferentTurnIndex(firstTurnIndex,turn);
	return i==-1?0:DIFFERENT_TURN2[firstTurnIndex]+i;
}

int Number::arrayIndex(CPtrTurnType array,int value){
	CPtrTurnType p=array;
	for(int i=0;*p!=-1;p++,i++){
		if(*p==value){
			return i;
		}
	}
	return -1;
}

