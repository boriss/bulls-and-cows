@echo off
REM use "run.bat N_THREADS"
REM or "run.bat"  run 4 threads

set count=%1
IF "%1"=="" set count=4
for /l %%c in (1,1,%count%) DO  (
	start /LOW /B executor %%c
)
