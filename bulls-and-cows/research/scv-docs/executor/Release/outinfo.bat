@echo off
echo %time%

setlocal enabledelayedexpansion
for /l %%c in (1,1,4) DO  (
	set filename=io%%c.txt
	if exist !filename! (
		call :print_head !filename!
	)
	if not exist !filename! (
		echo !filename! not found
	)
)
pause
exit /b

:print_head
setlocal EnableDelayedExpansion
set /a counter=0
for /f "delims=" %%i in (%1) do (
	if !counter! == 0 (
		set line="%%i"
		set t="%~t1"
	)
	set /a counter+=1
)
rem echo %line%
rem echo %counter%
set a=lower
if !counter! LSS 3 set a=equals

set line=%line:~7,38%
set t=%t:~12,5%
echo %line% [%t%] %a%
exit /b
