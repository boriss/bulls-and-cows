/*****************************************************************************************************************
created by Alexey Slovesnov copyright C/C++ 2011-doomsday

This is a jobs executor. Compiled file uses for "bcService" application

tested only for filled first 1-3 & 1-4 turns setup in jobs.txt file


running{
	./executor param ;where param is digit from 1 to 9 define number of thread
	./executor works with it's temprorary file io1.txt - io9.txt
}

under windows{
	executor uses file jobs.txt to read and mark jobs

	Every thread take job from file jobs.txt and mark it as 'taken'.
	After it uses it's own temprorary file (ex io1.txt) where program stores current iteration

	program will work untill all jobs will finish

}

under unix{
	executor uses file io1.txt (it's same with one line in jobs.txt file). 
	So everytime job will finish user should manually create new io1.txt file with task

	executor can work under cron
}

both OS{
	1 executor can work in many thread (with different params)

	2 program multirun safe. If 'executor 1' is running and user launch executor with same parameter 'executor 1', 
	then 2nd instance will exit. To run 2nd thread launch executor with different param	

	3	when current job finished program renames file io1.txt to format 02-Number-01-crush5.txt for gathering information
}

dont do job.txt file with more than 100 jobs!!!

to setup command line argument use menu project/settings debug tab "program arguments"
*****************************************************************************************************************/


#include "..\bcw\StdAfx.h"
#include "..\bcw\solver.h"
#include "..\bcw\number.h"
#include "..\bcw\turns.h"

#ifdef _UNIX
	#include <unistd.h>
  #include <stdarg.h>

	const char REN[]="mv";
#else
	#include <windows.h>
	const char REN[]="ren";

	//my version of libraries has no this #define
	#ifndef INVALID_SET_FILE_POINTER
		#define INVALID_SET_FILE_POINTER  0xFFFFFFFF
	#endif

#endif

#ifndef _BULLS_COWS_
	#error "executor supports only bulls and cows game type"
#endif

//after #include stdafx.h
#if defined(_LARMOUTH_) && !defined(_MINIMIZE_AVERAGE_)
	#error larmouth can be use only with _MINIMIZE_AVERAGE_
#endif

const char TAKEN[]="taken";
const char SOLVE[]="solve";
//TAKEN & SOLVE SHOULD HAVE SAME LENGTH!!!


const char JOBS[]="jobs.txt";


int ti[7];//indexes
int r[7];
int st;
int param=0;
clock_t gt;

int ti2;//turn index in turn2
CPtrTurnType pti3;// & ptr to turn3
CPtrTurnType pti2;// & ptr to turn2
int nextMax=MAXN;
int gcount=7;//depth count
int gmde=3;//max depth exact
char fn[512];//name of io file depends on param parameter (for unix absolute path used)

FILE*gf;

char path[512];//path

//write error to error file
void writeError( const char* format, ... ){
	va_list args;
  va_start (args, format);

	char s[128];
	sprintf(s,"%serror%d.txt",path,param);
	FILE*f=fopen(s,"a");
	if(f){
	  vfprintf (f, format, args);
		fclose(f);
	}

  va_end (args);
}


void fproceed(const char* string){
	if(string!=NULL){
		fseek(gf,0,SEEK_SET);
		fprintf(gf,"%s",string);
	}
	fclose(gf);
}

void renameFile(){
	int i;
	const int j=gmde;
	const int count=gcount;
	char s[512],s1[512];

	//dont use path for destination ren command DOESN'T allow it
	sprintf(s,"%d%d-",BULL(r[0]),COW(r[0]) );

	for(i=1;i<st-1;i++){
		sprintf(s1,"%04d-%d%d-",Number::array[ti[i]].Int(),BULL(r[i]),COW(r[i]) );
		strcat(s,s1);
	}

	if(j+2>=count){
		sprintf(s1,"exact");
	}
	else{
		if(j+1==count-2){
			sprintf(s1,"crush%d",j+1);
		}
		else{
			sprintf(s1,"crush%d%d",j+1,count-2);
		}
	}
	strcat(s,s1);

#ifdef _UNIX
	//need to use full path for unix mv command (12sept2011)
	sprintf(s1,"%s %s %s%s.txt",REN,fn,path,s	);
#else
	//here we cann't use full path in destionation!
	//dont use path for destination ren command DOESN'T allow it
	sprintf(s1,"%s %s %s.txt",REN,fn,s	);
#endif

	system(s1);
	
}

void getBCS(char*s,bool skipLastItem){
	int i;
	char s1[16];
	s[0]=0;
	for(i=0;i<st-1;i++){
		sprintf(s1,"%s%04d(%d,%d)",i==0?"":"+",Number::array[ti[i]].Int(),BULL(r[i]),COW(r[i]));
		strcat(s,s1);
	}
	if(!skipLastItem){
		sprintf(s1,"+%04d(*,*)",Number::array[ti[i]].Int());
		strcat(s,s1);
	}
}

/*
read data from file and store estimate to it if it's better
e=MAXN -indicates 1st run load(mx from file)

return 0  successfully finish solving the case
return 1  still need to solve this file
return -1 error detected
*/
int next(int& mx){
	char s[256],s1[256],os1[256];
	int i,k;
	bool finish=false;

	//printf("fn{%s}\n",fn);

	gf=fopen(fn,"r+");
	if(!gf){
		printf("io file not found\n");
		return -1;
	}
	fgets(s,256,gf);
	s1[0]=0;
	fgets(s1,256,gf);
	fclose(gf);
	gf=fopen(fn,"r+");

	//fprintf return error 32 if not close and reopen file

	int _t[7],_r[7];
	if(s[0]=='e' ){//error or ended
		fproceed(0);
		return -1;	
	}
	else if(s[0]=='f'){//finished
		finish=true;//need to read turns
	}

	//st ~ startk
	st=6+(finish?3:0);

	//solve 0123(0,2)+1234(0,1) 7 5 44
	for( k=st,i=0 ; k==st || s[k-1]=='+' ; i++,k+=10 ){
		_t[i]=atoi(s+k);
		_r[i]=BC(atoi(s+k+5),atoi(s+k+7));
	}

	st=i;
	if(s[0]!='#'){
		st++;
	}

	if(finish){
		//setup t&r
		for(i=0;i<st-1;i++){
			ti[i]=Number::findIndex(_t[i]);
			r[i]=_r[i];	
			if(ti[i]==-1){
				fproceed("error index not found!!!!");
				return -1;
			}
		}
		fclose(gf);
		
		//have to read from 2nd string
		gcount=atoi(s1+k-1);//reading global parameters
		gmde=atoi(s1+k+1);


		renameFile();
		return 0;
	}

	/*if(st<3){
		printf("error st<3!!!!\n");
		printf("s=[%s]\n");

		fproceed("error st<3!!!!");
		return -1;
	}*/

	if(mx==MAXN){//program just started
		gcount=atoi(s+k);//reading global parameters
		gmde=atoi(s+k+2);
		nextMax=mx=atoi(s+k+4);
		if(mx==MAXN){
			nextMax=mx=MAXN-1;//prevent error
		}

		//check that already copied
		if(strlen(s1)<5){

		//copy 1st string to2nd to prevent it from corruption(cause later we'll change 1st string)
			fseek(gf,0,SEEK_SET);
			fprintf(gf,"%s",s);
			for(i=strlen(s);i<70;i++){//append spaces ot the end of string
				fprintf(gf," ");
			}
			fprintf(gf,"\n//%s [count %dth mde=%d max=%d]",s,gcount,gmde,mx);
		}


		if(st==2){
			if(s[0]=='#'){
				//pti2=Number::findInTurn2(_t[1]);
				pti2=Number::firstDifferentTurnPtr(_t[1]);
				if(pti2==0){
					fproceed("error index2 not found!!!!");
					return -1;
				}				
			}
			else{
				pti2=TURN2;
				_t[st-1]=Number::array[*pti2].Int();	
			}
		}
		else if(st==3){
			//ti2=Number::findTurn2Index(_t[1]);
			ti2=Number::firstDifferentTurnIndex(_t[1]);
			if(ti2==-1){
				fproceed("error index2 not found!!!!");
				return -1;
			}
			if(s[0]=='#'){//label1
				//pti3=Number::findInTurn3(ti2,_t[2]);
				pti3=Number::secondDifferentTurnPtr(ti2,_t[2]);
				if(pti3==0){
					fproceed("error index3 not found!!!!");
					return -1;
				}				
			}
			else{//1st run
				pti3=TURN3[ti2];
				//setup to prevent error on next cycle
				_t[2]=Number::array[*pti3].Int();
			}
		}
		else{
			if(s[0]=='#'){
				//_t already set
			}
			else{
				_t[st-1]=Number::array[MAXN-1].Int();	
			}
		}
		for(i=0;i<st;i++){
			//printf("fi %d %04d\n",i,_t[i]);
			ti[i]=Number::findIndex(_t[i]);
			r[i]=_r[i];	
			if(ti[i]==-1){
				fproceed("error index not found!!!!");
				return -1;
			}
		}
		fproceed(0);
		return 1;
	}

	//setup next sequence
	getBCS(os1,false);

	//printf("=os[%s]\n",s);
	if(st==2){
		pti2++;
		ti[st-1]=*pti2;
	}
	else if(st==3){
		pti3++;
		ti[st-1]=*pti3;
	}
	else{
		ti[st-1]--;
	}

	//should be called after setup next turn
	if(ti[st-1]==0 || st==3 && *pti3==-1 || st==2 && *pti2==-1 || mx==0){
		finish=true;
	}

	//update first row
	fseek(gf,0,SEEK_SET);
	if(finish){
		//	if(s[0]=='e' || s[0]=='f'){//error or ended
		//if change this string need to change end condition as well
		getBCS(s1,true);
		sprintf(s,"finished %s e=%d th=%d     ",s1,mx,param);//need to append symbols at the end to clear prev string
	}
	else{
		getBCS(s1,false);
		//FIRST SYMBOL # USE FOR RECOGNIZING SEE LABEL1
		sprintf(s,"##### %s %1d %1d %4d",s1,gcount,gmde,mx);
	}
	fprintf(gf,s);	

	if(mx<nextMax){
		//write solved t
		fseek(gf,0,SEEK_END);
		int dt=int((clock()-gt)/(double)CLOCKS_PER_SEC);
		fprintf(gf,"\n##### %s %4d %4d time %02d:%02d:%02d",os1,nextMax,mx,dt/3600,(dt/60)%60,dt%60);
		nextMax=mx;//store to global variable
	}


	fclose(gf);

	if(finish){//rename file
		renameFile();
	}

	//printf("pr");
	//getc(stdin);
	return !finish;
}

#ifndef _UNIX
//return if we still need to solve something
bool getJob(){

	#define RET if(h!=INVALID_HANDLE_VALUE){CloseHandle(h);}return

	//1st check io file
	sprintf(fn,"%sio%d.txt",path,param);
	gf=fopen(fn,"r+");
	if(gf){
		printf("%s file exists (not error)\n",fn);
		fclose(gf);
		return true;//still need to proceed io file
	}

	const int SZBUFF=40*1000;
	char buff[SZBUFF+1];//+1 byte to setup last item to 0
	char *p,*p1;
	BOOL bError;
	DWORD nBytes;
	const int SZ_JOB=128;
	char job[SZ_JOB];
	HANDLE h;
	DWORD lastError;

	//2nd got new job

	do{
		printf("try to get access to jobs file th%d\n",param);
		sprintf(buff,"%s%s",path,JOBS);
		h=CreateFile(buff,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
		if(h==INVALID_HANDLE_VALUE){
			lastError=GetLastError();
			if(lastError==ERROR_SHARING_VIOLATION){
				printf("have to wait sleep 3 seconds th%d\n",param);
				Sleep(3000);
			}
			else{
				writeError("CreateFile ERROR=0x%x\n",lastError);
				RET false;		
			}
		}
	}while(h==INVALID_HANDLE_VALUE);

	printf("got access to jobs file th%d\n",param);

	buff[SZBUFF]=0;
	bError = ReadFile( h, buff, SZBUFF,&nBytes, NULL); 
	if(bError==FALSE){
		lastError=GetLastError();
		writeError("ReadFile ERROR=0x%x\n",lastError);
		RET false;
	}
	buff[nBytes]=0;//new 11jul

	p=strstr(buff,SOLVE);
	if(p==NULL){//no more jobs
		RET false;		
	}

	p1=strchr(p,'#');
	if(p1==NULL){
		writeError("ERROR # not found at the end of job[%s]\n",p);
		RET false;
	}

	//update JOBS
	if(p1-p>=SZ_JOB){
		writeError("ERROR p1-p>=SZ_JOB \n");
		RET false;
	}

	strncpy(job,p,p1-p);
	job[p1-p]=0;

	bError = SetFilePointer( h,int(p-buff), NULL,    FILE_BEGIN ); 
	if ( bError == INVALID_SET_FILE_POINTER && GetLastError() != NO_ERROR ){//from MSDN
		lastError=GetLastError();
		writeError("SetFilePointer ERROR=0x%x\n",lastError);
		RET false;
	} 

	bError = WriteFile( h, TAKEN, strlen(TAKEN),&nBytes, NULL); 
	if(bError==FALSE){
		lastError=GetLastError();
		writeError("write file ERROR=0x%x\n",lastError);
		RET false;
	}

	//setup job only after success execution with JOBS file
	gf=fopen(fn,"w+");//need to write errors
	fprintf(gf,"%s",job);
	fclose(gf);
	//printf("fn[%s][%s]\n",fn,job);
	
	RET true;
#undef RET	
}
#endif //#ifndef _UNIX

int main(int argc, char **argv){
	if(argc<=1){
		printf("error to few arguments\n");
		return -3;
	}
	param=atoi(argv[1]);//define from which file we should read info
	if(param==0){
		printf("error atoi returns 0\n");
		return -4;
	}
	if(param<0){
		printf("error param should be >0\n");
		return -4;
	}

	int i,e,nextRes;
	int mx;
	int t[7];
	char out[4096];
	bool outR[MBC];
	char*pchar;

#ifndef _UNIX
	//get absolute path
	GetModuleFileName( NULL, path, 512 );
  pchar=strrchr(path,'\\')+1;
  *pchar=0;
#endif

#ifdef _UNIX
  sprintf(path,argv[0]);
  pchar=strrchr(path,'/');
	if(pchar==NULL){
		path[0]=0;
	}
	else{
		pchar++;
		*pchar=0;
	}

	//check for another instance
	sprintf(fn,"%spid%d.txt",path,param);
	gf=fopen(fn,"r");
	if(gf){
		fread(out,1,512,gf);
		fclose(gf);
		pchar=strstr(out,"pid=");
		if(pchar==NULL){
			printf("error pid substring not found\n");
			return -5;
		}
		e=atoi(pchar+4);

		sprintf(out,"/proc/%d/cmdline",e);
    if (access(out, F_OK) != -1) {
			printf("already running\n");
			return -2;
    }
	}

	//store pid for unix
	sprintf(fn,"%spid%d.txt",path,param);
	gf=fopen(fn,"wb+");
	if(!gf){
		return 2;
	}

	time_t     now;
  struct tm  *ts;
  now = time(0);
  ts = localtime(&now);
  strftime(out, sizeof(out)-1, " %d-%b-%Y %H:%M:%S\n", ts);
	fprintf(gf,"started at %s pid=%d",out,getpid() );

	fclose(gf);

#else
	HANDLE hMutex;//mutex prevents multirunning with same param
	sprintf(out,"executor%d_969C164C-C4E8-4ff6-BF7D-EB0A4C98F127",param);
	hMutex=CreateMutex(NULL, FALSE,out);
	if(hMutex==NULL && GetLastError()==ERROR_ACCESS_DENIED || WaitForSingleObject(hMutex,0)==WAIT_TIMEOUT){
		printf("already running\n");
		if(hMutex!=NULL){
			CloseHandle(hMutex);
		}
		return -2;
	}
	if(hMutex==NULL){
		printf("hMutex==NULL le=%d %x\n",GetLastError(),GetLastError() );
		return -3;
	}

#endif

	Number::init();
	Solver::init();

#ifdef _UNIX
	sprintf(fn,"%sio%d.txt",path,param);
#endif

#ifndef _UNIX
	while(getJob()){
#endif
		for( mx=MAXN ; (nextRes=next(mx))==1 ; ){//do job

			for(i=0;i<st;i++){
				t[i]=Number::array[ti[i]].Int();
			}

#ifdef _MINIMIZE_AVERAGE_
			bool bc40=
#endif
			Solver::subgroups(NULL,outR, t,r,st-1,1 );

			for(i=0;i<MBC;i++){
				if(outR[i]){
					break;
				}
			}
			if(i==MBC){
				continue;
			}

			gt=clock();
			e=0;
#ifdef _MINIMIZE_AVERAGE_
			if(bc40){
				e+=st;
			}
#endif

			for(i=0;i<MBC;i++){
				if(!outR[i]){
					continue;
				}
				r[st-1]=i;

				e+=Solver::estimate(0,mx-e, gcount, t, r, st,gmde,false );
				if(e>=mx){
					break;
				}
			}

			if(e<mx){
				mx=e;
				//ms=0 checking in next function because need to write result at first
			}
		}//for(mx)
#ifndef _UNIX
		if(nextRes!=0){//error
			break;
		}
	}//while getJob
#endif
//end:
	Number::deInit();
	Solver::deInit();

#ifdef _UNIX
#else
	ReleaseMutex(hMutex);
	CloseHandle(hMutex);
#endif

	printf("finish th%d\n",param);
	return 0;

}
