//this uses to check running of threads in unix
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
const int maxThreads=6;
int main(){
  FILE*f;
  int i,pid,j;
  char s[512],orig[512];
  char*p;

	time_t     now;
  struct tm  *ts;
  now = time(0);
  ts = localtime(&now);
  strftime(s, sizeof(s)-1, "%d-%b-%Y %H:%M:%S\n", ts);
	printf("date/time = %s",s );
  
  for(i=1;i<=maxThreads;i++){
    //check both files
    //printf("[i=%d]",i);
    sprintf(s,"io%d.txt",i);
    f=fopen(s,"rb");
    if(f==NULL){
      break;
    }
    fclose(f);

    sprintf(s,"pid%d.txt",i);
    f=fopen(s,"rb");
    if(f==NULL){
      break;
    }
    
    j=fread(orig,1,512,f);
		fclose(f);
    orig[j]=0;
		p=strstr(orig,"pid=");
		if(p==NULL){
			printf("error pid substring not found i=%d\n",i);
			return -5;
		}
		pid=atoi(p+4);
    sprintf(s,"/proc/%d/cmdline",pid);
    printf("pid%d=%5d %s ",i,pid,access(s, F_OK) != -1 ? "running":"not run");
    
    sprintf(s,orig);
    for(p=s;*p!='\r' && *p!='\n' && *p!=0;p++);
    *p=0;
    printf("%s ",s+12);
    
    sprintf(s,"io%d.txt",i);
    
    f=fopen(s,"rb");
    
    j=fread(orig,1,512,f);
    
		fclose(f);
    
    orig[j]=0;
    
    sprintf(s,orig);
    
    p=strrchr(s,'*');
    if(p==NULL){
      printf(" * not found",s+6);      
    }
    else{
      p[2]=0;
      printf("%s",s+6);      
    }
    
    printf("\n");
    //printf("[i=%dend]",i);

  }
  return 0;
}
